<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function book() {
      return $this->hasMany(Book::class);
    }

    public function purchase() {
      return $this->hasMany(Purchase::class);
    }

    public function wishlist(){
       return $this->hasMany(Wishlist::class);
    }

    public function review(){
       return $this->hasMany(Review::class);
    }

}
