<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model{

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function wishlist(){
     return $this->hasMany(Wishlist::class);
  }

  public function purchase(){
     return $this->belongsTo(Purchase::class);
  }

  //queryscope lista corsi
  public function scopeCourses($query){
      return $query->select('course')
               ->where('user_id', '1')
               ->distinct()
               ->get();;
  }

  //queryscope mostra i primi 8 libri
  public function scopeShowFirtsEight($query, $condition){
      return $query->orderBy('created_at', 'desc')
               ->where('condition', $condition)
               ->take('8')
               ->get();
  }

}
