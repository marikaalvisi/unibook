<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;


class CartController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //aggiornamento quantità carrello nel caso in cui  la disponibilità effettiva sia minori di quelle precedentemente inserite
      $cart = session()->get('cart');
      if($cart){
        foreach(session('cart') as $id => $details){
         $book = Book::findorfail($id);
         if($details["quantity"] > $book->availability){
           $cart[$id]["quantity"] = $book->availability;
           session()->put('cart', $cart);
         }
        }
      }
      return view('shopping_cart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store an item with item id = $id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->ajax()){
       $id = $request->get('id');

      $book = Book::find($id);
      if(!$book) {
          abort(404);
      }
      $cart = session()->get('cart');

      // if cart is empty then this the first product
      if(!$cart) {
          $cart = [
                  $id => [
                      "title" => $book->title,
                      "author" => $book->author,
                      "quantity" => 1,
                      "price" => $book->price,
                      "image" => $book->image,
                  ]
          ];
          session()->put('cart', $cart);
          return response()->json(["articles"=>count(session('cart'))]);

      }

      // if cart not empty then check if this product exist then increment quantity
      if(isset($cart[$id])) {
          if(($book->availability) < $request->quantity){
            return redirect()->back()->with('error', 'Ci dispiace, non abbiamo a disposizione tutte queste copie!');
          }
          else{
            $cart[$id]['quantity']++;
            session()->put('cart', $cart);
            return response()->json(["articles"=>count(session('cart'))]);
          }
      }

      // if item not exist in cart then add to cart with quantity = 1
      $cart[$id] = [
        "title" => $book->title,
        "author" => $book->author,
        "quantity" => 1,
        "price" => $book->price,
        "image" => $book->image
      ];
      session()->put('cart', $cart);
      return response()->json(["articles"=>count(session('cart'))]);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      if($request->id and $request->quantity){
          $book = Book::find($request->id);
          $cart = session()->get('cart');

          //se la quantità disponibile è minore di quella richiesta
          if($book->availability < $request->quantity){
            $cart[$request->id]["quantity"] = $book->availability;
            session()->put('cart', $cart);

            //calcolo subtotale complessivo carrello
            $subtotal = 0;
            $spedizione= 0;
            $total = 0;
            foreach(session('cart') as $id => $details){
              $subtotal += $details['price'] * $details['quantity'];
            }
            if ($subtotal < 50){
              $spedizione = 7.50;
              $total = $subtotal + $spedizione;
            }
            else{
              $spedizione = 5.00;
              $total = $subtotal + $spedizione;
            }

            //aggiunta due cifre decimali e valuta
            $subtotal = number_format((float)$subtotal , 2, '.', '')."€";
            $total = number_format((float)$total , 2, '.', '')."€";
            $spedizione = number_format((float)$spedizione , 2, '.', '')."€";
            return response()->json(["state"=>'false', "value"=>$cart[$request->id]["quantity"], "subtotal"=>$subtotal, "total"=>$total, "spedizione"=>$spedizione]);
          }


          else{
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);

            //calcolo subtotale complessivo carrello
            $subtotal = 0;
            $spedizione= 0;
            $total = 0;
            foreach(session('cart') as $id => $details){
              $subtotal += $details['price'] * $details['quantity'];
            }
            if ($subtotal < 50){
              $spedizione = 7.50;
              $total = $subtotal + $spedizione;
            }
            else{
              $spedizione = 5.00;
              $total = $subtotal + $spedizione;
            }

            //aggiunta due cifre decimali e valuta
            $subtotal = number_format((float)$subtotal , 2, '.', '')."€";
            $total = number_format((float)$total , 2, '.', '')."€";
            $spedizione = number_format((float)$spedizione , 2, '.', '')."€";

            return response()->json(["state"=>'true', "value"=>$cart[$request->id]["quantity"], "subtotal"=>$subtotal, "total"=>$total, "spedizione"=>$spedizione]);
          }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      if($request->id) {
          $cart = session()->get('cart');
          if(isset($cart[$request->id])) {
              unset($cart[$request->id]);
              session()->put('cart', $cart);
          }
          //calcolo subtotale complessivo carrello
          $subtotal = 0;
          $spedizione= 0;
          $total = 0;
          foreach(session('cart') as $id => $details){
            $subtotal += $details['price'] * $details['quantity'];
          }
          if ($subtotal < 50){
            $spedizione = 7.50;
            $total = $subtotal + $spedizione;
          }
          else{
            $spedizione = 5.00;
            $total = $subtotal + $spedizione;
          }

          //aggiunta due cifre decimali e valuta
          $subtotal = number_format((float)$subtotal , 2, '.', '')."€";
          $total = number_format((float)$total , 2, '.', '')."€";
          $spedizione = number_format((float)$spedizione , 2, '.', '')."€";
          return response()->json(["state"=>'true', "articles"=>count(session('cart')), "subtotal"=>$subtotal, "total"=>$total, "spedizione"=>$spedizione]);

      }
      return response()->json(["state"=>'false']);
    }
}
