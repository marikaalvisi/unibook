<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Book;
use App\User;


class BookController extends Controller{



    //mostra la lista di libri
    public function index(){
        $courses = Book::Courses();
        return view('books', compact('courses'));
     }


     //mostra la dashboard utente o admin in base al ruolo dell'utente loggato
     public function dashboard(){
         $courses = Book::Courses();
         if (Auth::user()->role == 'admin')
           return view('dashboard');
         else
           return view('mybooks', compact('courses'));

      }



    //mostra il form per la creazione di un nuovo libro
    public function create(){
        return view('createBook');
    }



    //memorizza un nuovo libro
    public function storeUser(Request $request){

      $validatedData = $request->validate([
        'title' => 'required|max:255',
        'author' => 'required|max:255',
        'course' => 'required|max:255',
        'price' => 'required|numeric|between:1,999.99',
        'editor' => 'required|max:255',
        'edition' => 'required|max:255',
        'pages' => 'required|integer|between:1,9999',
        'availability' => 'max:5',
        'description' => 'max:4096',
      ]);

      $input = $request->all();
      $new = new Book();

      $new->title = $input['title'];
      $new->author = $input['author'];
      $new->course = $input['course'];
      if (isset($input['description'])){
        if($input['description'] != "")
          $new->description = $input['description'];
      }
      $input['price'] = floatval(number_format(floatval($input['price']), 2, '.', ''));
      $new->price = $input['price'];
      if (isset($input['image'])){
        $image = $request->file('image');
        $destionationPath = public_path('/images');
        $input['image'] = time() . '.' . $image->getClientOriginalExtension();
        $image->move($destionationPath, time() . '.' . $image->getClientOriginalExtension());
        $new->image = $input['image'];
      }
      $new->editor = $input['editor'];
      $new->edition = $input['edition'];
      $new->pages = $input['pages'];
      if (isset($input['availability'])){
        $new->availability = $input['availability'];
      }
      $new->user_id = Auth::user()->id;
      $new->condition = 'used';

      $new->save();

      return redirect('/myaccount/mybooks')->with('success','Elemento aggiunto con successo');
      }




      //memorizza un nuovo libro
      public function storeAdmin(Request $request){

        $validatedData = $request->validate([
          'title' => 'required|max:255',
          'author' => 'required|max:255',
          'course' => 'required|max:255',
          'price' => 'required|numeric|between:0,999.99',
          'editor' => 'required|max:255',
          'edition' => 'required|max:255',
          'pages' => 'required|integer|between:1,9999',
          'availability' => 'min:0',
          'description' => 'max:4096',
        ]);

        $input = $request->all();
        $new = new Book();

        $new->title = $input['title'];
        $new->author = $input['author'];
        $new->course = $input['course'];
        if (isset($input['description'])){
          if($input['description'] != "")
          $new->description = $input['description'];
        }
        $input['price'] = floatval(number_format(floatval($input['price']), 2, '.', ''));
        $new->price = $input['price'];
        if (isset($input['image'])){
          $image = $request->file('image');
          $destionationPath = public_path('/images');
          $input['image'] = time() . '.' . $image->getClientOriginalExtension();
          $image->move($destionationPath, time() . '.' . $image->getClientOriginalExtension());
          $new->image = $input['image'];
        }
        $new->editor = $input['editor'];
        $new->edition = $input['edition'];
        $new->pages = $input['pages'];
        if (isset($input['availability'])){
          $new->availability = $input['availability'];
        }
        $new->user_id = 1;
        $new->condition = 'new';

        $new->save();

        return redirect('/dashboardBooks')->with('success','Elemento aggiunto con successo');
      }



    //mostra uno specifico libro con i relativi libri usati
    public function show($id){
      $book = Book::findorfail($id);
      $title = $book->title;
      $usedBooks = Book::where('title', '=', $title)
                    ->where('condition', '=', 'used')
                    ->where('availability', '>', '0')
                    ->get();
      $courses = Book::Courses();

     return view('book', compact('book', 'usedBooks', 'courses'));
    }



    //mostra gli ultimi 8 libri inseriti
    public function showNew (){
      $books = Book::ShowFirtsEight('new');
      $usedBooks = Book::ShowFirtsEight('used');
      return view('index', compact('books', 'usedBooks'));
    }


    //mostra il form per la modifica di un libro
    public function edit($id){
      $book = Book::find($id);
      $courses = Book::Courses();

     if(Auth::user()->role == "admin")
      return view('editBookAdmin', compact('book', 'courses'));
     else
      return view('editBookUser', compact('book', 'courses'));
    }


    //aggiorna una specifica risorsa nel database
    public function updateUser(Request $request, $id){

      $validatedData = $request->validate([
        'title' => 'required|max:255',
        'author' => 'required|max:255',
        'course' => 'required|max:255',
        'price' => 'required|numeric|between:0,999.99',
        'editor' => 'required|max:255',
        'edition' => 'required|max:255',
        'pages' => 'required|integer|between:1,9999',
        'description' => 'max:4096',
      ]);

      $new = Book::find($id);
      $input = $request->all();

      if($input['title'] != ""){
        $new->title = $input['title'];
      }
      if($input['author'] != ""){
        $new->author = $input['author'];
      }
      if($input['course'] != ""){
        $new->course = $input['course'];
      }
      if($input['description'] != ""){
        $new->description = $input['description'];
      }
      if($input['price'] != ""){
        $input['price'] = floatval(number_format(floatval($input['price']), 2, '.', ''));
        $new->price = $input['price'];
      }
      if(isset($input['image'])){
        $image = $request->file('image');
        $destionationPath = public_path('/images');
        $input['image'] = time() . '.' . $image->getClientOriginalExtension();
        $image->move($destionationPath, time() . '.' . $image->getClientOriginalExtension());
        $new->image = $input['image'];
      }
      if($input['editor'] != ""){
        $new->editor = $input['editor'];
      }
      if($input['edition'] != ""){
        $new->edition = $input['edition'];
      }
      if($input['year'] != ""){
        $new->year = $input['year'];
      }
      if($input['pages'] != ""){
        $new->pages = $input['pages'];
      }
      if(isset($input['availability'])){
        $new->availability = $input['availability'];
      }

      $new->save();
      return redirect('/myaccount/mybooks')->with('success','Elemento aggiornato con successo');
    }



      //aggiorna una specifica risorsa nel database
      public function updateAdmin(Request $request, $id){

        $validatedData = $request->validate([
          'title' => 'required|max:255',
          'author' => 'required|max:255',
          'course' => 'required|max:255',
          'price' => 'required|numeric|between:0,999.99',
          'editor' => 'required|max:255',
          'edition' => 'required|max:255',
          'pages' => 'required|integer|between:1,9999',
          'description' => 'max:4096',
          'availability' => 'min:0',
        ]);

        $new = Book::find($id);
        $input = $request->all();

        if($input['title'] != ""){
          $new->title = $input['title'];
        }
        if($input['author'] != ""){
          $new->author = $input['author'];
        }
        if($input['course'] != ""){
          $new->course = $input['course'];
        }
        if($input['description'] != ""){
          $new->description = $input['description'];
        }
        if($input['price'] != ""){
          $input['price'] = floatval(number_format(floatval($input['price']), 2, '.', ''));
          $new->price = $input['price'];
        }
        if(isset($input['image'])){
          $image = $request->file('image');
          $destionationPath = public_path('/images');
          $input['image'] = time() . '.' . $image->getClientOriginalExtension();
          $image->move($destionationPath, time() . '.' . $image->getClientOriginalExtension());
          $new->image = $input['image'];
        }
        if($input['editor'] != ""){
          $new->editor = $input['editor'];
        }
        if($input['edition'] != ""){
          $new->edition = $input['edition'];
        }
        if($input['year'] != ""){
          $new->year = $input['year'];
        }
        if($input['pages'] != ""){
          $new->pages = $input['pages'];
        }
        if(isset($input['availability'])){
          $new->availability = $input['availability'];
        }

        $new->save();

        return redirect('dashboardBooks')->with('success','Elemento aggiornato con successo');
        }




    //rimuove una speficifica risorsa dal database
    public function destroy($id)  {
      $book = Book::find($id);
      $book->delete();

      if (Auth::user()->role == 'admin')
        return redirect('dashboardBooks')->with('success','Elemento rimosso con successo');
      else
        return redirect('myaccount/mybooks')->with('success','Elemento rimosso con successo');
    }



    //live search books
    function action(Request $request){
     if($request->ajax())
     {

       $query = '%';
       $course = '%';
       $condition = '%';

       if($request->get('query') != ''){
         $query = $request->get('query');
       }

       if($request->get('course') != 'Qualsiasi'){
         $course = $request->get('course');
       }

       if($request->get('condition') != 'Qualsiasi'){
         if($request->get('condition') == 'Nuovo'){
           $condition = 'new';
         }
         if($request->get('condition') == 'Usato'){
           $condition = 'used';
         }
       }

       $data = Book::orderBy('title', 'asc')
         ->where('course', 'like', $course)
         ->where('condition', 'like', $condition)
         ->where('availability', '>', '0')
         ->where('title', 'like', '%'.$query.'%')
         ->paginate(8);


      return view('pagination_data')->with('data',$data);
     }
    }




    //live search dashboard admin
    function actionDashboard(Request $request){

     if($request->ajax()){
      $output = '';
      $query = $request->get('query');

      if($query != ''){
       $data = Book::orderBy('title', 'asc')
         ->where('id', 'like', '%'.$query.'%')
         ->orWhere('title', 'like', '%'.$query.'%')
         ->orWhere('author', 'like', '%'.$query.'%')
         ->orWhere('course', 'like', '%'.$query.'%')
         ->where('condition', 'new')
         ->paginate(8);
      }

      if($query == ''){
        $data = Book::orderBy('title', 'asc')
                ->where('user_id', '1')
                ->paginate(8);
      }
      return view('pagination_dashboard')->with('data',$data);
     }
    }




    //live search dashboard user
    function actionDashboardUser(Request $request){

     if($request->ajax()){
      $output = '';
      $query = $request->get('query');

      if($query != ''){
       $data = Book::orderBy('title', 'asc')
         ->where('title', 'like', '%'.$query.'%')
         ->orWhere('author', 'like', '%'.$query.'%')
         ->orWhere('course', 'like', '%'.$query.'%')
         ->where('user_id', Auth::user()->id)
         ->get();
      }

      if($query == ''){
        $data = Book::orderBy('title', 'asc')
                ->where('user_id', Auth::user()->id)
                ->get();
      }
      return view('pagination_user_dashboard')->with('data',$data);
    }
  }

}
