<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Book;
use Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $input = $request->all();

       $nome = $input['nome'];
       $cognome = $input['cognome'];

       $user = new User($nome , $cognome);

       return view('userCreate', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $new = new User();

      $new->name       = $input['name'];
      $new->email      = $input['email'];
      $new->password   = $input['password'];

      $new->save();

      return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::find($id);
      $userBooks = Book::where('user_id', '=', $id)
                   ->get();

      return view('user', compact('user', 'userBooks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile($id)
    {
      $user = User::find($id);

      return view('editProfile', ['user' => $user]);
    }

    public function editEmail($id)
    {
      $user = User::find($id);

      return view('editEmail', ['user' => $user]);
    }

    public function editPassword($id)
    {
      $user = User::find($id);

      return view('editPassword', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      /*$validatedData = $request->validate([
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6|confirmed'

      ]);*/

      $new = User::find($id);
      $input = $request->all();

      if(isset($input['username'])){
        $new->username = $input['username'];
      }

      if(isset($input['name'])){
        $new->name = $input['name'];
      }

      if(isset($input['surname'])){
        $new->surname = $input['surname'];
      }

      if(isset($input['image'])){
        $new->image = $input['image'];
      }

      if(isset($input['email'])){
        $new->email = $input['email'];
      }

      if(isset($input['password'])){
        $user->password = bcrypt(request('password'));
      }

      $new->save();
      return redirect()->back()->with('success','Dati aggiornati con successo!');
    }


    public function updateEmail(Request $request, $id)
    {

      if (!(Hash::check($request->get('password'), Auth::user()->password))) {
            return redirect('/myaccount/credentials')->with("error","Password inserita non valida");
        }

      if(strcmp($request->get('email'), Auth::user()->email) == 0){
          return redirect('/myaccount/credentials')->with("error","Email attuale ed email nuova concidono");
      }

      $this->validate(request(), [
           'email' => 'required|email|unique:users',
           'password' => 'required'
       ]);


      $new = User::find($id);
      $input = $request->all();
      $new->email = $input['email'];

      $new->save();

      return redirect('/myaccount/credentials')->with("success","Email aggiornata con successo!");
    }



    public function updatePassword(Request $request, $id)
    {

      if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect('/myaccount/credentials')->with("error","Password attuale inserita non valida");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            return redirect('/myaccount/credentials')->with("error","Password attuale e password nuova concidono");
        }

      $validatedData = $request->validate([
        'current-password' => 'required',
        'new-password' => 'required|string|min:6|confirmed',
      ]);

      $new = User::find($id);
      $new->password = bcrypt($request->get('new-password'));

      $new->save();

      return redirect('/myaccount/credentials')->with("success","Password aggiornata con successo!");
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
