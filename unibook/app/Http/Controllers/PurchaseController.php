<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Book;
use App\Purchase;
use App\User;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Auth::user()->role == 'admin'){
        return view('dashboardOrders');
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //form per l'inserimento dell'indirizzo
    public function editAddress (){
      return view('address');
    }

    //inserimento in una sessione dell'indirizzo di consegna (non è necessario memorizzarlo nel database)
    public function addAddress(Request $request)
    {
        $validatedData = $request->validate([
          'name' => 'required|max:50',
          'surname' => 'required|max:50',
          'address' => 'required|max:50',
          'CAP' => 'required|integer|digits:5',
          'city' => 'required|max:50',
        ]);

        $input = $request->all();

        $address = [
                "name" =>  $input['name'],
                "surname" => $input['surname'],
                "address" => $input['address'],
                "CAP" => $input['CAP'],
                "city" => $input['city'],
        ];

        $request->session()->put('address', $address);

        return redirect('/checkout/payment');
    }

    //form per l'inserimento dell'indirizzo
    public function editPayment (){
      return view('payment');
    }


    //inserimento in una sessione dell'indirizzo di consegna o dei dati della carta di credito(non è necessario memorizzarlo nel database)
    public function addPayment(Request $request)
    {
      if(isset($request['payment'])){
        if($request['payment'] == "creditcard"){
          $validatedData = $request->validate([
            'name' => 'required|max:50',
            'cardnumber' => 'required|integer|digits:16',
            'date' => 'required|max:5',
            'CVV' => 'required|integer|digits:3',
          ]);

          $payment = [
                   "type" =>  'Carta di credito',
                   "name" =>  $request['name'],
                   "cardnumber" =>  $request['cardnumber'],
                   "date" => $request['date'],
                   "CVV" => $request['CVV'],
           ];
           $request->session()->put('payment', $payment);
           return redirect('/checkout/confirm');
        }

        if($request['payment'] == "money"){
           $payment = ["type" =>  'In contanti alla consegna'];
           $request->session()->put('payment', $payment);
           return redirect('/checkout/confirm');
        }
      }
      return redirect('/checkout/payment');
    }

    public function resume (){
      return view('confirm');
    }

    //diminuzione quantità libri disponibili e aggiunta ordine in memoria
    public function confirm(Request $request)
    {
        $purchase = new Purchase();
        $purchase->user_id = Auth::user()->id;
        $subtotal = 0;

        foreach(session('cart') as $id => $details){
          $book = Book::find($id);
          $quantity = $book->availability - $details['quantity'];
          Book::where('id', $id)->update(array('availability' => $quantity));
          //inserimento nel db dell'ordine
          $subtotal += $details['price'] * $details['quantity'];
          $purchase->order = $purchase->order."@".$details['title']." x".$details['quantity'];
        }

        if ($subtotal < 50){
          $spedizione = 7.50;
          $total = $subtotal + $spedizione;
        }
        else{
          $spedizione = 5.00;
          $total = $subtotal + $spedizione;
        }
        $purchase->total = $total;
        $purchase->save();

        //eliminazione delle sessioni relative a questo acquisto
        Session::forget('cart');
        Session::forget('address');
        Session::forget('payment');
        return redirect('myaccount/myorders')->with('success','Acquisto completato con successo');;
    }

    //mostra gli ordini dell'utente loggato
    public function showOrders(){

      $orders =  Purchase::orderBy('created_at', 'desc')
               ->where('user_id', Auth::user()->id)
               ->get();
      return view('myorders', compact('orders'));
    }


    //mostra tutti gli ordini
    public function action(Request $request){

      if($request->ajax())
      {
       $query = $request->get('query');

       if($query != ''){
        $data = Purchase::orderBy('created_at', 'desc')
                ->where('id', 'like', '%'.$query.'%')
                ->paginate(2);
       }

       if($query == ''){
         $data = Purchase::orderBy('created_at', 'desc')
                  ->paginate(2);
       }
       return view('pagination_orders_admin')->with('data', $data);
      }
    }

    //modifica dello stato di un ordine
    public function updateOrder(Request $request, $id){
      if(isset($request['status'])){
        $order = Purchase::find($id);
        $status = $request['status'];
        Purchase::where('id', $id)->update(array('order_status' => $status));
      }
      return redirect('dashboardOrders');
    }

}
