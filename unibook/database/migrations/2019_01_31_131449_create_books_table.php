<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('books', function (Blueprint $table) {
          $table->increments('id'); //id
          $table->timestamps();
          $table->string('title'); //titolo
          $table->string('author'); //autore
          $table->string('course'); //corso
          $table->string('condition')->default('new'); //condizioni nuovo/usato
          $table->string('description', 4096)->default('Nessuna descrizione disponibile'); //descrizione
          $table->float('price'); //prezzo
          $table->string('image')->default('it-default-big_default.jpg'); //copertina
          $table->string('editor'); //editore
          $table->string('edition'); //edizione
          $table->integer('pages'); //pagine
          $table->integer('user_id')->default('1'); 
          $table->foreign('user_id')->references('id')->on('users');
          $table->integer('availability')->default(1); //disponibilità
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
