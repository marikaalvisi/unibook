<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /*Schema::create('purchases', function (Blueprint $table) {
          $table->increments('id'); //id ordine
          $table->timestamps();
          $table->string('id_user'); //è univoco, rappresenta l'id dell'acquirente
          $table->integer('id_book'); //è univoco, rappresenta l'id del libro acquistato
          $table->date('date_of_purchase'); //data d'acquisto
      });*/

      Schema::create('purchases', function (Blueprint $table) {
          $table->increments('id'); //id ordine
          $table->integer('user_id'); //è univoco, rappresenta l'id dell'acquirente
          $table->foreign('user_id')->references('id')->on('users');
          $table->string('order'); //stringa contenente la lista degli id dei libri acquistati
          $table->float('total'); //prezzo totale
          $table->string('order_status')->default('in elaborazione'); //possibili stati: in elaborazione, in spedizione, consegnato
          $table->timestamps();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
