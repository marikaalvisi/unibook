<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Course;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      course::create([
          'course' => 'Informatica',
          'image' => 'informatics.jpeg',
      ]);

      course::create([
          'course' => 'Matematica',
          'image' => 'math.jpeg',
      ]);

      course::create([
          'course' => 'Economia',
          'image' => 'economy.jpeg',
      ]);

      course::create([
          'course' => 'Fisica',
          'image' => 'physic.jpeg',
      ]);

      course::create([
          'course' => 'Geologia',
          'image' => 'geology.jpeg',
      ]);

      course::create([
          'course' => 'Diritto',
          'image' => 'law.jpeg',
      ]);
    }
}
