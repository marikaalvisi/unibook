<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      category::create([
          'category' => 'Informatica',
          'image' => 'informatics.jpeg',
      ]);

      category::create([
          'category' => 'Matematica',
          'image' => 'math.jpeg',
      ]);

      category::create([
          'category' => 'Economia',
          'image' => 'economy.jpeg',
      ]);

      category::create([
          'category' => 'Fisica',
          'image' => 'physic.jpeg',
      ]);

      category::create([
          'category' => 'Geologia',
          'image' => 'geology.jpeg',
      ]);

      category::create([
          'category' => 'Diritto',
          'image' => 'law.jpeg',
      ]);
    }
}
