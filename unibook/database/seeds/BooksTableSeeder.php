<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      book::create([
          'title' => 'Il linguaggio C. Fondamenti e tecniche di programmazione. Ediz. mylab. Con espansione online',
          'author' => 'Paul J. Deitel,Harvey M. Deitel',
          'course' => 'Informatica',
          'condition' => 'new',
          'description' => 'Dopo un’ampia trattazione introduttiva sui sistemi informatici e sullo stato dell’arte delle tecnologie informatiche, l’approccio metodologico si snoda partendo dalle tecniche di programmazione strutturata e procedendo con i costrutti del linguaggio che la supportano, fino a mostrare, con numerosi e corposi esempi ed esercizi, come costruire effettivamente sistemi software anche complessi. Nel corso della trattazione vengono affrontate ampiamente e in modo operativo le tematiche relative agli algoritmi e alle strutture di dati, fondamentali per un approccio professionale alla programmazione.',
          'price' => '39.00',
          'editor' => 'Pearson',
          'edition' => '8',
          'pages' => '618',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Economia e gestione delle imprese',
          'author' => 'Franco Fontana,Matteo Caroli',
          'course' => 'Economia',
          'condition' => 'new',
          'description' => "Questo manuale ha l'obiettivo di fornire in maniera organica i concetti portanti e gli strumenti operativi della gestione strategica dell'impresa, attuata nella prospettiva degli obiettivi della sostenibilità. In tale prospettiva, chiarite le caratteristiche dell'impresa come sistema complesso, il testo approfondisce come essa interagisce nel suo ambiente competitivo, influenzata anche dalle condizioni di contesto più generali. Illustra poi il concetto e le funzioni della strategia nell'evoluzione dell'impresa e i contenuti delle strategie competitive, di collaborazione e di crescita. Questi temi sono poi utilizzati anche nella prospettiva dell'elaborazione del business model e del processo di pianificazione e posti in relazione con il patrimonio di risorse e competenze distintive dell'impresa.",
          'price' => '38.00',
          'editor' => 'McGraw-Hill Education',
          'edition' => '5',
          'pages' => '534',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Principi di economia politica. Teoria ed evidenza empirica. Ediz. mylab. Con espansione online',
          'author' => 'Daron Acemoglu,David Laibson,John A. Lis',
          'course' => 'Economia',
          'condition' => 'new',
          'description' => "Il manuale è pensato per tutti gli studenti che frequentano un primo corso di economia politica. Spesso i ragazzi hanno l’impressione che l’economia consista in una serie di modelli e affermazioni teoriche con scarse basi empiriche. L’approccio evidence based degli autori vuole ribaltare questa impressione: usando i dati, gli autori spiegano in che modo gli economisti valutano e migliorano le intuizioni della scienza economica; i dati, inoltre, rendono i concetti più facili da ricordare. L’uso dell’evidenza empirica aiuta gli studenti a sviluppare l’intuizione, perché i dati spostano il discorso da principi astratti a fatti concreti. Ogni capitolo è una dimostrazione del ruolo chiave che l’evidenza empirica gioca nell’avanzamento della scienza economica.",
          'price' => '53.00',
          'editor' => 'Pearson',
          'edition' => '6',
          'pages' => '772',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Microeconomia',
          'author' => 'David A. Besanko,Ronald R. Braeutigam',
          'course' => 'Economia',
          'condition' => 'new',
          'description' => "David Besanko e Ronald Breautigam, micreoconomisti accreditati in tutto il mondo, ritengono che il metodo di insegnamento più efficace sia quello di presentare i contenuti attraverso un'ampia gamma di interessanti Applicazioni, corredando il tutto con un elevato numero di esercizi svolti e problemi. Questo tipo di approccio consente agli studenti di vedere concretamente l'interazione fra i concetti chiave della materia, per afferrare compiutamente questi ultimi attraverso la pratica e capire come questi si applichino effettivamente ai mercati e alle imprese. La particolare enfasi posta su esercizi svolti e problemi è ciò che caratterizza questo volume rispetto agli altri testi di microeconomia. Gli esercizi svolti, inclusi nel testo di ogni capitolo, servono a guidare l'allievo attraverso esempi numerici specifici. Tali esercizi sono poi integrati con rappresentazioni grafiche e con la trattazione verbale, di modo che gli allievi possano vedere chiaramente, con l'uso di numeri e relazioni funzionali, il significato dei grafici e dei concetti che si vuole loro trasmettere. Il testo si rivolge ai corsi di Microeconomia a Economia, Giurisprudenza, Scienze politiche e Scienze statistiche. La nuova edizione del volume è stata rivista nelle soluzioni dell'eserciziario di fine volume e include un nuovo capitolo sull'economia comportamentale, inserito nella parte sette.",
          'price' => '61.00',
          'editor' => ' McGraw-Hill Education',
          'edition' => '3',
          'pages' => '642',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Manuale di diritto privato',
          'author' => 'Andrea Torrente,Piero Schlesinger',
          'course' => 'Diritto',
          'condition' => 'new',
          'description' => "Questa ventitreesima edizione del Manuale di diritto privato persegue l'obiettivo di offrire al lettore un quadro aggiornato dell'ordinamento civilistico in considerazione della continua e rapida evoluzione del sistema giuridico. Numerose le novità legislative intervenute dall'ultima edizione, in particolare in ambiti fondativi della struttura della società e dei rapporti tra persone ricordiamo la L. 20 maggio 2016, n. 76, sulle unioni civili e le convivenze, la L. 6 giugno 2016, n. 106 in tema di riforma del cd. terzo settore e dell'impresa sociale, la L. 8 marzo 2017, n. 24 in tema di responsabilità medica. In ambito di rapporti economici si segnalano, per quanto riguarda la tutela del credito, il D.L. 3 maggio 2016, n. 59 (convertito con L. 22 maggio 2017, n. 81) che ha introdotto l'istituto del pegno mobiliare non possessorio; in materia di tutela del lavoro autonomo, l. 22 maggio 2017, n. 81; in tema di società a partecipazione pubblica, il D.lgs. 19 agosto 2016, n. 175. Si è voluto dar conto esaustivamente della disciplina dei diversi istituti, per comporre un compiuto quadro d'insieme, confermando altresì l'attenzione alla giurisprudenza di legittimità.",
          'price' => '63.00',
          'editor' => 'Giuffrè',
          'edition' => '23',
          'pages' => '1568',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Diritto pubblico',
          'author' => 'Roberto Bin,Giovanni Pitruzzella',
          'course' => 'Diritto',
          'condition' => 'new',
          'description' => "Il testo è aggiornato ai più importanti avvenimenti istituzionali degli ultimi mesi, e in particolare: la legge elettorale approvata nel 2017; le vicende della formazione del Governo Conte; la riforma del regolamento del Senato; la questione dei vitalizi; il caso Taricco.",
          'price' => '48.00',
          'editor' => 'Giappichelli',
          'edition' => '16',
          'pages' => '600',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Elementi di fisica. Vol. 1: Meccanica, termodinamica.',
          'author' => 'Paolo Mazzoldi,Massimo Nigro,Cesare Voci',
          'course' => 'Fisica',
          'condition' => 'new',
          'description' => "Nessuna descrizione disponibile.",
          'price' => '33.00',
          'editor' => 'Edises',
          'edition' => '2',
          'pages' => '472',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Elementi di fisica. Vol. 1: Meccanica, termodinamica.',
          'author' => 'Paolo Mazzoldi,Massimo Nigro,Cesare Voci',
          'course' => 'Fisica',
          'condition' => 'new',
          'description' => "Nessuna descrizione disponibile.",
          'price' => '33.00',
          'editor' => 'Edises',
          'edition' => '2',
          'pages' => '472',
          'user_id' => '1',
          'availability' => '10'
      ]);

      book::create([
          'title' => 'Principi di fisica',
          'author' => 'Raymond A. Serway,John W. Jewett',
          'course' => 'Fisica',
          'condition' => 'new',
          'description' => "Il testo ha due obiettivi principali: fornire allo studente una presentazione chiara e logica dei concetti e rafforzare la comprensione dei principi fisici mediante l'approfondimento di interessanti applicazioni nel mondo reale. L’approccio didattico in questa edizione si avvale della combinazione di diverse caratteristiche, quali l’eliminazione di alcuni argomenti della fisica classica, l’introduzione di alcuni argomenti della fisica del XX secolo (relatività ristretta, quantizzazione dell'energia e modello di Bohr dell'atomo di idrogeno) e la connessione dei principi di fisica ad interessanti applicazioni biomediche, a conseguenze di interesse sociale, ai fenomeni naturali e ai progressi tecnologici. Le novità più importanti di questa quinta edizione sono numerose. Innanzitutto sono stati aggiunti nuovi contesti, l’organizzazione di quesiti e problemi è stata rinnovata in modo da consentire di sviluppare diverse capacità logiche e interpretative, che esulano da un contesto matematico per trovare maggiore applicazione nella vita quotidiana. L’approccio modellistico è stato inoltre migliorato, consentendo allo studente di ricondurre ogni problema a un principio fondamentale associato a un modello semplificato. Nuovi contenuti sono stati aggiunti agli esempi guidati, per sviluppare le capacità di ragionamento e facilitare la comprensione dei principi. È stata condotta, inoltre, un’estesa revisione della grafica, per rendere le immagini più esplicative.",
          'price' => '55.00',
          'editor' => 'Edises',
          'edition' => '5',
          'pages' => '1032',
          'user_id' => '1',
          'availability' => '10'
      ]);
    }
}
