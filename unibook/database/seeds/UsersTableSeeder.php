<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      user::create([
          'id' => '1',
          'username' => 'admin',
          'email' => 'admin@unibook.it',
          'password' => '$2y$10$b5VqYLogwloBGutWRzKuXu4D25b3yo9ODK1QvRAC4yKnYjUow2iCa',
          'role' => 'admin'
      ]);
    }
}
