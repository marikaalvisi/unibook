<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	//routes accessibili sempre//////////////////////////////////////////////////

	Route::get('/login', function(){
		return view('login');
	});

	Route::get('/goodbye', function(){
		return view('logout');
	});

	Route::get('/book', function(){
		return view('book');
	});

	Route::get('/contactus', function(){
		return view('contact');
	});

	Route::get('/infodelivery', function(){
		return view('delivery');
	});

	Route::get('/whoare', function(){
		return view('whoare');
	});

	Route::get('/aboutus', 'ReviewController@index');

	//mostra libri nuovi
	Route::get('/', 'BookController@showNew');

	Route::get('/books', 'BookController@index');
	//live search
	Route::get('/books/action', 'BookController@action');

	//visualizzare un singolo libro
	Route::get('/books/{id}', 'BookController@show');

	//visualizzare un utente
	Route::get('/books/{id}/userinfo', 'UserController@show');

	Auth::routes();


	//routes accessibili solo se si è admin/////////////////////////////////////
	Route::group([
		'middleware' => ['web', 'auth', 'mustBeAdmin'],
		], function(){

		Route::get('/home', 'HomeController@index')->name('home');

		Route::delete('remove-book', 'BookController@removeBook');

		Route::get('/dashboardBooks', 'BookController@dashboard');
		//live search dashboard admin
		Route::get('/dashboardBooks/action', 'BookController@actionDashboard');

		//creare un nuovo libro admin
		Route::get('/dashboardBooks/new', 'BookController@create');
		Route::post('/dashboardBooks/new', 'BookController@storeAdmin');

		//cancellare un libro admin
		Route::get('/dashboardBooks/{id}/destroy', 'BookController@destroy');

		//modificare un libro admin
		Route::get('/dashboardBooks/{id}/edit', 'BookController@edit');

		//aggiornare un libro admin
		Route::post('/dashboardBooks/{id}/update', 'BookController@updateAdmin');

		//visualizzazione ordini admin
		Route::get('/dashboardOrders', 'PurchaseController@index');
		Route::get('/dashboardOrders/action', 'PurchaseController@action');

		//modificare un ordine admin
		Route::post('/dashboardOrders/{id}/update', 'PurchaseController@updateOrder');

	});



	//routes accessibili solo se si è user///////////////////////////////////////
	Route::group([
		'middleware' => ['web', 'auth', 'mustBeUser'],
		], function(){

		Route::get('/home', 'HomeController@index')->name('home');

		Route::delete('/remove-book', 'BookController@removeBook');

		//live search dashboard user
		Route::get('/myaccount/mybooks/action', 'BookController@actionDashboardUser');

		//creare un nuovo libro user
		Route::get('/myaccount/mybooks/new', 'BookController@create');
		Route::post('/myaccount/mybooks/new', 'BookController@storeUser');

		//cancellare un libro user
		Route::get('/myaccount/mybooks/{id}/destroy', 'BookController@destroy');

		//modificare un libro user
		Route::get('/myaccount/mybooks/{id}/edit', 'BookController@edit');

		//aggiornare un libro user
		Route::post('/myaccount/mybooks/{id}/update', 'BookController@updateUser');

		Route::get('/myaccount', function(){
			return view('myaccount');
		});

		Route::get('/myaccount/profile', function(){
			return view('profile');
		});

		Route::get('/myaccount/credentials', function(){
			return view('credentials');
		});

		Route::get('/myaccount/mybooks', 'BookController@dashboard');

		Route::get('/myaccount/myorders', 'PurchaseController@showOrders');

		//modificare un utente
		Route::get('/profile/edit', 'UserController@edit');
		Route::get('/myaccount/profile/{id}/edit', 'UserController@editProfile');
		Route::get('/myaccount/credentials/{id}/edit/email', 'UserController@editEmail');
		Route::get('/myaccount/profile/{id}/edit/password', 'UserController@editPassword');

		//aggiornare un utente
		Route::post('/myaccount/profile/{id}/update', 'UserController@update');
		Route::post('/myaccount/credentials/{id}/updateEmail', 'UserController@updateEmail');
		Route::post('/myaccount/credentials/{id}/updatePassword', 'UserController@updatePassword');

		//CARRELLO/////////////////////////////////////////////////////////////////
		Route::get('/cart', 'CartController@index');
		Route::get('/add-to-cart', 'CartController@store');
		Route::post('/update-cart', 'CartController@update');
		Route::delete('/remove-from-cart', 'CartController@destroy');

		//WISHLIST////////////////////////////////////////////////////////////////
		Route::get('/wishlist', 'WishlistController@index');
		Route::get('/add-to-wishlist', 'WishlistController@store');
		Route::delete('remove-from-wishlist', 'WishlistController@destroy');

		//CHECKOUT/////////////////////////////////////////////////////////////////
		//inserimento indirizzo di spedizione
		Route::get('/checkout/address', 'PurchaseController@editAddress');
		Route::post('/checkout/address', 'PurchaseController@addAddress');

		//inserimento dati pagamento
		Route::get('/checkout/payment', 'PurchaseController@editPayment');
		Route::post('/checkout/payment', 'PurchaseController@addPayment');

		//conferma ordine
		Route::get('/checkout/confirm', function(){
			return view('confirm');
		})->middleware('auth');
		//Route::get('/checkout/confirm', 'PurchaseController@resume');
		Route::get('/checkout/success', 'PurchaseController@confirm');

		//RECENSIONI//////////////////////////////////////////////////////////////
		Route::post('/aboutus', 'ReviewController@store');
	});
