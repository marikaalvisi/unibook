@extends ('layout.app')
@section('content')

  <!--se il libro è nuovo-->
  @if($book->condition == 'new')
    <!--foto e info libro-->
    <div class="container shadow p-3 mb-5 rounded"style="background-color:white">
      <br>
      <div class="row">
        <div class="col-sm" >
        <!--foto libro-->
          <img src="{{ url('images/'.$book->image) }}" class="rounded float-left" width="300" height="400">
        </div>

        <div class="col-sm">
          <!--info libro-->
          <table class="table table-sm">
            <tbody>
              <tr>
                <th scope="row">Titolo</th>
                <td>{{$book->title}}</td>
              </tr>
              <tr>
                <th scope="row">Autore</th>
                <td>{{$book->author}}</td>
              </tr>
              <tr>
                <th scope="row">Editore</th>
                <td>{{$book->editor}}</td>
              </tr>
              <tr>
                <th scope="row">Edizione</th>
                <td>{{$book->edition}}</td>
              </tr>
              <tr>
                <th scope="row">Pagine</th>
                <td>{{$book->pages}}</td>
              </tr>
              <tr>
                <th scope="row">Disponibilità</th>
                <td>{{$book->availability}}</td>
              </tr>
              <tr>
                <th scope="row">Prezzo</th>
                <td>{{ number_format((float)$book->price , 2, '.', '')}}€</td>
              </tr>
            </tbody>
          </table>

          <br>
          <br>


          <!--pulsanti-->
          <div class="container">
            <div class="row">

              <div class="grow">
                <button class="btn btn-primary add-to-cart" data-id="{{ $book->id }}">Metti nel carrello <i data-feather="plus"></i></button>
              </div>
              <div class="bigger">
                <button class="btn btn-link a-color add-to-wishlist" data-id="{{ $book->id }}" style="color: #ff8c00"><i data-feather="heart"></i></button>
              </div>
            </div>
            <br>
            <br>
            @if (Auth::user())
              <div class="row">
                <h6 style="color:gray">Hai una copia usata di questo libro da vendere?<br>Clicca su "Vendi questo libro" per pubblicarlo al prezzo che preferisci.<br>Gestiscilo dalla sezione "i miei libri"</h6>
              </div>
              <div class="row">
                <div class="grow">
                  <button type="button" class="btn btn-secondary" style="text-decoration:none" data-toggle="modal" data-target="#exampleModalScrollable">
                    Vendi questo libro <i data-feather="plus"></i>
                  </button>
                </div>
              </div>
            @endif
          </div>
      </div>
    </div>
    <br>
  </div>

  <br>

  <!--descrizione libro-->
  <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
    <br>
    <div style="clear:both;">
        <h4>Descrizione</h4>
        <p>{{$book->description}}</p>
    </div>
    <br>
  </div>


  <br>

  <!--libri usati-->
  <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
    <br>
    <div style="clear:both;">
        <h4>Libri usati</h4>
        <p style="color:gray">In questa sezione trovi le versioni usate di {{$book->title}} messe in vendita da altri utenti come te.<br>
        Non preoccuparti, ci occuperemo noi della consegna e del controllo delle sue condizioni,
        compra senza pensieri!</p>
        @if(count($usedBooks) != "0")
          @foreach($usedBooks as $usedBook)
                  <hr>
                  <div class="row">
                    <div class="col-sm">
                    <!--foto libro-->
                      <img src="{{ url('images/'.$usedBook->image) }}" class="rounded float-left" width="200" height="300">
                    </div>
                    <div class="col-sm" style="margin:auto">
                      <table class="table table-sm">
                        <tbody>
                          <tr>
                            <th scope="row">Prezzo</th>
                            <td>{{ number_format((float)$usedBook->price , 2, '.', '')}}€</td>
                          </tr>
                          <tr>
                            <th scope="row">Descrizione</th>
                            <td colspan="2">{{$usedBook->description}}</td>
                          </tr>
                          <tr>
                            <th scope="row">Venduto da</th>
                            <td colspan="2">{{$usedBook->user->username}}</td>
                          </tr>
                        </tbody>
                      </table>
                      <br>
                      <!--pulsanti-->
                      <div class="container">
                        <div class="row">
                          <div class="grow">
                            <button class="btn btn-primary add-to-cart" data-id="{{ $usedBook->id }}">Metti nel carrello <i data-feather="plus"></i></button>
                          </div>
                          <div class="bigger">
                            <button class="btn btn-link a-color add-to-wishlist" data-id="{{ $usedBook->id }}" style="color: #ff8c00"><i data-feather="heart"></i></button>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <h6 style="color:gray">Hai bisogno di ricevere qualche informazione in più?</h6>
                        </div>
                        <div class="row">
                          <div class="bigger">
                          <a href="{{ URL::action('UserController@show', $usedBook->user->id) }}" class="a-color bigger" style="color:#ff8c00; text-decoration:none">
                              Contatta il venditore<i data-feather="phone"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
        @endforeach
      @endif

      @if(count($usedBooks) == "0")
        <p>Nessun libro usato disponibile</p>
      @endif

    </div>
    <br>
  </div>

  <br>




<!--se il libro è usato-->
  @else
  <!--foto e info libro-->
  <div class="container shadow p-3 mb-5 rounded"style="background-color:white">
    <br>
    <div class="row">
        <div class="col-sm" >
        <!--foto libro-->
          <img src="{{ url('images/'.$book->image) }}" class="rounded float-left" width="300" height="400">
        </div>

        <div class="col-sm">
          <!--info libro-->
          <table class="table table-sm">
            <tbody>
              <tr>
                <th scope="row">Titolo</th>
                <td>{{$book->title}}</td>
              </tr>
              <tr>
                <th scope="row">Autore</th>
                <td>{{$book->author}}</td>
              </tr>
              <tr>
                <th scope="row">Editore</th>
                <td>{{$book->editor}}</td>
              </tr>
              <tr>
                <th scope="row">Edizione</th>
                <td>{{$book->edition}}</td>
              </tr>
              <tr>
                <th scope="row">Pagine</th>
                <td>{{$book->pages}}</td>
              </tr>
              <tr>
                <th scope="row">Prezzo</th>
                <td>{{$book->price}}€</td>
              </tr>
              <tr>
                <th scope="row">Venduto da</th>
                <td colspan="2">{{$book->user->username}}</td>
              </tr>
            </tbody>
          </table>

          <br>
          <br>


          <!--pulsanti-->
          <div class="container">
            <div class="row">
              <div class="grow">
                <button class="btn btn-primary add-to-cart" data-id="{{ $book->id }}">Metti nel carrello <i data-feather="plus"></i></button>
              </div>
              <div class="bigger">
                <button class="btn btn-link a-color add-to-wishlist" data-id="{{ $book->id }}" style="color: #ff8c00"><i data-feather="heart"></i></button>
              </div>
            </div>
            <br>
            <div class="row">
              <h6 style="color:gray">Hai bisogno di ricevere qualche informazione in più?</h6>
            </div>
            <div class="row">
              <div class="bigger">
                <a href="{{ URL::action('UserController@show', $book->user->id) }}" class="a-color bigger" style="color:#ff8c00; text-decoration:none">
                  Contatta il venditore<i data-feather="phone"></i>
                </a>
              </div>
            </div>
          </div>
          <br>
      </div>
    </div>
    <br>
  </div>

  <br>

  <!--descrizione libro-->
  <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
    <br>
    <div style="clear:both;">
        <h4>Descrizione</h4>
        <p>{{$book->description}}</p>
    </div>
    <br>
  </div>


  <br>

  <!--libri usati-->
  <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
    <br>
    <div style="clear:both;">
        <h4>Altre versioni usate di {{$book->title}}</h4>
        @if(count($usedBooks) > "1")
          @foreach($usedBooks as $usedBook)
                  <hr>
                  <div class="row">
                    <div class="col-sm">
                    <!--foto libro-->
                      <img src="{{ url('images/'.$book->image) }}" class="rounded float-left" width="200" height="300">
                    </div>
                    <div class="col-sm" style="margin:auto">
                      <table class="table table-sm">
                        <tbody>
                          <tr>
                            <th scope="row">Prezzo</th>
                            <td>{{$usedBook->price}}€</td>
                          </tr>
                          <tr>
                            <th scope="row">Descrizione</th>
                            <td colspan="2">{{$usedBook->description}}</td>
                          </tr>
                        </tbody>
                      </table>
                      <br>
                      <!--pulsanti-->
                      <div class="container">
                        <div class="row">
                          <div class="grow">
                            <button class="btn btn-primary add-to-cart" data-id="{{ $book->id }}">Metti nel carrello <i data-feather="plus"></i></button>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <h6 style="color:gray">Hai bisogno di ricevere qualche informazione in più?</h6>
                        </div>
                        <div class="row">
                          <div class="bigger">
                            <a href="{{ URL::action('UserController@show', $usedBook->owner) }}" class="a-color bigger" style="color:#ff8c00; text-decoration:none">
                              Contatta il venditore<i data-feather="phone"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
        @endforeach
      @endif

      @if(count($usedBooks) == "1")
        <p>Nessun altro libro usato trovato</p>
      @endif

    </div>
    <br>
  </div>
  <br>
  @endif


  <!-- Modal -->
  <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalScrollableTitle">Nuovo libro</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          @if (count($errors) > 0)
            <script>
              Swal.fire({
                type: 'warning',
                title: 'Oops...',
                text: 'Sembra che tu non abbia inserito correttamente i campi!',
                showConfirmButton: false,
                timer: 1500,
              })
            </script>
          @endif

          <form action="{{ URL::action('BookController@storeUser') }}" method="POST" id="form">

              {{ csrf_field() }}

              <!--titolo-->
              <div class="form-group">
                  <label for="title">Titolo <strong style="color:#ff8c00">*</strong></label>
                  <input type="text" class="form-control" id="title"  name="title" value="{{$book->title}}">
              </div>

              <!--autore-->
              <div class="form-group">
                  <label for="author">Autore <strong style="color:#ff8c00">*</strong></label>
                  <input type="text" class="form-control" id="author" name="author" value="{{$book->author}}">
              </div>

              <div class="form-group">
                <label for="course">Corso di studi</label>
                <select class="form-control" name="course" id="course">
                  @foreach($courses as $course)
                    <option>{{$course->course}}</option>
                  @endforeach
                  <option>Altro</option>
                </select>
              </div>

              <!--immagine-->
              <div class="form-group">
                <label for="image">Immagine</label>
                <input type="file" class="form-control-file" id="image" name="image">
              </div>

              <!--Descrizione-->
              <div class="form-group">
                <label for="description">Descrizione</label>
                <textarea class="form-control" id="description" name="description"></textarea>
              </div>

              <!--prezzo-->
              <div class="form-group">
                  <label for="author">Prezzo <span style="color:gray">(Prezzo massimo: 999.99€)</span><strong style="color:#ff8c00">*</strong></label>
                  <input type="text" class="form-control" id="price" name="price">
              </div>

              <!--editore-->
              <div class="form-group">
                  <label for="author">Editore <strong style="color:#ff8c00">*</strong></label>
                  <input type="text" class="form-control" id="editor" name="editor" value="{{$book->editor}}">
              </div>

              <!--edizione-->
              <div class="form-group">
                  <label for="author">Edizione <strong style="color:#ff8c00">*</strong></label>
                  <input type="text" class="form-control" id="edition" name="edition" value="{{$book->edition}}">
              </div>

              <!--pagine-->
              <div class="form-group">
                  <label for="author">Pagine <strong style="color:#ff8c00">*</strong></label>
                  <input type="text" class="form-control" id="pages" name="pages" value="{{$book->pages}}">
              </div>

              <div class="grow">
                <button type="submit" class="btn btn-primary">Metti in vendita <i data-feather="plus"></i></button>
              </div>

              <br>

              <div class="grow">
                <a class="btn btn-secondary" href="{{ URL::action('BookController@dashboard') }}" data-dismiss="modal" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
              </div>

          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
