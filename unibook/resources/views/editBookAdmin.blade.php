@extends('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <div class="container">
    <div class="row">
        <h3>Modifica i dati di "{{ $book->title }}"</h3>

        <div class="col-sm-6">

            @if (count($errors) > 0)
              <script>
                Swal.fire({
                  type: 'warning',
                  title: 'Oops...',
                  text: 'Sembra che tu non abbia inserito correttamente i campi!',
                  showConfirmButton: false,
                  timer: 1500,
                })
              </script>
            @endif

            <form action="{{ URL::action('BookController@updateAdmin', $book->id) }}" method="POST" id="form" enctype="multipart/form-data">

                {{ csrf_field() }}

                <!--titolo-->
                <div class="form-group">
                    <label for="title">Titolo</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$book->title}}">
                </div>

                <!--autore-->
                <div class="form-group">
                    <label for="author">Autore</label>
                    <input type="text" class="form-control" id="author" name="author" value="{{$book->author}}">
                </div>


                <!--corso-->
                <div class="form-group">
                    <label for="author">Corso di studi</label>
                    <input type="text" class="form-control" id="course" name="course" value="{{$book->course}}">
                </div>

                <!--immagine-->
                <div class="form-group">
                  <label for="image">Immagine</label>
                  <input type="file" id="image" name="image" value="{{$book->image}}">
                </div>

                <!--Descrizione-->
                <div class="form-group">
                  <label for="description">Descrizione</label>
                  <textarea class="form-control" id="description" name="description" value="{{$book->description}}"></textarea>
                </div>

                <!--prezzo-->
                <div class="form-group">
                    <label for="author">Prezzo <span style="color:gray">(Prezzo massimo: 999.99€)</span></label>
                    <input type="text" class="form-control" id="price" name="price" value="{{$book->price}}">
                </div>

                <!--editore-->
                <div class="form-group">
                    <label for="author">Editore</label>
                    <input type="text" class="form-control" id="editor" name="editor" value="{{$book->editor}}">
                </div>

                <!--edizione-->
                <div class="form-group">
                    <label for="author">Edizione</label>
                    <input type="text" class="form-control" id="edition" name="edition" value="{{$book->edition}}">
                </div>

                <!--anno-->
                <div class="form-group">
                    <label for="author">Anno</label>
                    <input type="text" class="form-control" id="year" name="year" value="{{$book->year}}">
                </div>

                <!--pagine-->
                <div class="form-group">
                    <label for="author">Pagine</label>
                    <input type="text" class="form-control" id="pages" name="pages" value="{{$book->pages}}">
                </div>

                <!--condizioni-->
                  <!--<label for="condition">Condizioni</label>
                  <br>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="condition" id="condition" value="new">
                    <label class="form-check-label" for="condition">Nuovo</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="condition" id="condition" value="used">
                    <label class="form-check-label" for="condition">Usato</label>
                  </div>-->

                <!--Disponibilità-->
                <div class="form-group">
                    <label for="author">Pezzi attualmente disponibili</label>
                    <input type="text" class="form-control" id="availability" name="availability" value="{{$book->availability}}">
                </div>

                <div class="grow">
                  <button type="submit" class="btn btn-primary">Aggiorna <i data-feather="refresh-cw"></i></button>
                </div>

                <br>
                <div class="grow">
                  <a class="btn btn-secondary" href="{{ URL::action('BookController@dashboard') }}" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
                </div>

              </form>
        </div>
    </div>
  </div>
</div>

@endsection
