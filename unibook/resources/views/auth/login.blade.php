@extends('layout.app')

@section('content')
<div class="container ">
  <div class="shadow p-3 mb-5 bg-white rounded">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="container box"  style="background-color:white">
            <br>
            <h4 align="center">Accedi</h4><br />

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                  <!--<span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>-->
                                    <script>
                                      Swal.fire({
                                        type: 'warning',
                                        title: "Ops, email o password inserita non valida!",
                                        timer: 1500,
                                        showConfirmButton: false,
                                      })
                                    </script>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                  <!--<span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>-->
                                    <script>
                                      Swal.fire({
                                        type: 'warning',
                                        title: "Ops, password inserita non valida!",
                                        timer: 1500,
                                        showConfirmButton: false,
                                      })
                                    </script>
                                @endif
                            </div>
                        </div>

                        <!--<div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Ricordami') }}
                                    </label>
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                              <div class="row">
                                <div class="col">
                                  <div class="grow">
                                    <button type="submit" class="btn btn-primary">  {{ __('Accedi') }}
                                      <i data-feather="log-in"></i></button>
                                   </div>
                                </div>
                                <div class="col">
                                  @if (Route::has('password.request'))
                                      <a class="btn btn-link a-color" href="{{ route('password.request') }}" style="color:black">
                                          {{ __('Password dimenticata?') }}
                                      </a>
                                  @endif
                                </div>
                              </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
