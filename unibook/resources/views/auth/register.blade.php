@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="container box shadow p-3 mb-5 rounded"  style="background-color:white">
            <br>

            <h4 align="center">Registrati</h4><br />

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <!--<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>-->
                                    <script>
                                      Swal.fire({
                                        type: 'warning',
                                        title: "Ops, questo username è già in uso!",
                                        timer: 1500,
                                        showConfirmButton: false,
                                      })
                                    </script>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <!--<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>-->
                                    <script>
                                      Swal.fire({
                                        type: 'warning',
                                        title: "Ops, sembra che esista già un account associato a questa email!",
                                        timer: 1500,
                                        showConfirmButton: false,
                                      })
                                    </script>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <!--<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>-->
                                    <script>
                                      Swal.fire({
                                        type: 'warning',
                                        title: "Password non valida! Deve contenere almeno 6 caratteri",
                                        timer: 1500,
                                        showConfirmButton: false,
                                      })
                                    </script>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Conferma Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                              <div class="grow">
                                <button type="submit" class="btn btn-primary">  {{ __('Registrati') }}
                                  <i data-feather="log-in"></i></button>
                               </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
