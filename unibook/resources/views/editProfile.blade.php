@extends('layout.app')

@section('content')


<div class="container" style="background-color:white">
  <br>
    <div class="container">
      <h3>Modifica i miei dati</h3>
      <form action="{{ URL::action('UserController@update', $user->id) }}" method="POST">

          {{ csrf_field() }}

          <!--nome-->
          <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username" value="{{$user->username}}">
          </div>

          <!--nome-->
          <div class="form-group">
              <label for="name">Nome</label>
              <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
          </div>

          <!--nome-->
          <div class="form-group">
              <label for="surname">Cognome</label>
              <input type="text" class="form-control" id="surname" name="surname" value="{{$user->surname}}">
          </div>

          <!--immagine-->
          <div class="form-group">
            <label for="image">Immagine</label>
            <input type="file" class="form-control-file" id="image" name="image">
          </div>


          <div class="grow">
            <button type="submit" class="btn btn-primary">Aggiorna</button>
          </div>

          <br>

          <div class="grow">
            <a href="{{ URL::asset('myaccount/profile') }}" style="color:black; text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
          </div>

      </form>
  </div>
  <br>
</div>

@endsection
