@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div align="center">
    <h3>Informazioni Spedizione</h3>
  </div>
  <br>

  <table class="table">
    <thead>
      <tr>
        <th scope="col"></th>
        <th scope="col">Spese di spedizione</th>
        <th scope="col">Tempi di consegna</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">Ordini inferiori a 50€</th>
        <td>7.50€</td>
        <td>5-7 giorni lavorativi</td>
      </tr>
      <tr>
        <th scope="row">Ordini superiori a 50€</th>
        <td>5.00€</td>
        <td>5-7 giorni lavorativi</td>
      </tr>
    </tbody>
  </table>

<br>
<div class="alert alert-light" role="alert">
  Spedizione disponibile solo in Italia<br>
  Le spese di spedizione per i libri usati sono comprensive delle spese per il ritiro all'acquirente e consegna al destinatario.
</div>
<br>
</div>

@endsection
