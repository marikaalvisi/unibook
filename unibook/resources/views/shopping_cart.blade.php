@extends ('layout.app')
@section('content')

<div class="container shopping_cart">
  <!--se è presente qualcosa nel carrello-->
  @if (count(session('cart')) > 0)
  <div class="row">
    <!--carrello-->
    <div class="col-md-9">
      <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
        <div class="container">
          <h3>Il tuo carrello</h3>
          @php
           $subtotal = 0;
          @endphp
          <!--se il carrello NON è vuoto-->
            <h6 style="color:gray">Articoli nel carrello: <span id="quantity_cart">{{count(session('cart')) }}<span></h6>
                @foreach(session('cart') as $id => $details)
                  <div class=" element{{$id}}">
                      <hr>
                      @php
                        $subtotal += $details['price'] * $details['quantity']
                      @endphp

                      @if($details['quantity'] > 0)
                        <table id="cart" class="table-responsive">
                          <thead>
                          <tr>
                              <th scope="col" style="width:45%"></th>
                              <th scope="col" style="width:20%" class="text-center">Prezzo</th>
                              <th scope="col" style="width:10%">Quantità</th>
                              <th scope="col" style="width:25%"></th>
                          </tr>
                          </thead>
                          <tbody>
                            <tr>
                               <td data-th="Prodotto">
                                   <div class="row">
                                       <div class="col"><img src="{{url('images/'.$details['image'])}}" width="110" height="150"></div>
                                       <div class="col">
                                         <div class="row">
                                           <h6>{{ $details['title'] }}</h6>
                                         </div>
                                         <div class="row">
                                           <p style="color:gray">{{ $details['author'] }}</p>
                                         </div>
                                       </div>
                                   </div>
                               </td>
                               <td data-th="Prezzo" class="text-center">{{ number_format((float)$details['price'], 2, '.', '')}}€</td>
                               <td data-th="Quantità" >
                                   <input type="number" value="{{ $details['quantity'] }}" data-id="{{ $id }}" class="form-control quantity update-cart" min="1">
                               </td>
                               <td class="actions" data-th="" align="center">
                                   <button class="btn btn-link remove-from-cart bigger a-color" style="color:gray;text-decoration:none" data-id="{{ $id }}">Rimuovi<i data-feather="trash-2"></i></button>
                               </td>
                           </tr>
                       </tbody>
                     </table>
                     @else
                       <table id="cart" class="table-responsive" style="padding:6px">
                         <thead>
                         <tr>
                             <th scope="col" style="width:40%"></th>
                             <th scope="col" style="width:30%"></th>
                             <th scope="col" style="width:20%"></th>
                         </tr>
                         </thead>
                         <tbody>
                           <tr>
                              <td data-th="Prodotto">
                                  <div class="row">
                                      <div class="col"><img src="{{url('images/'.$details['image'])}}" width="100" height="150"></div>
                                      <div class="col">
                                          <h6>{{ $details['title'] }}</h6>
                                          <p style="color:gray">{{ $details['author'] }}</p>
                                      </div>
                                  </div>
                              </td>
                              <td>
                                <h6>Ops, sembra che l'articolo non sia più disponibile!</h6>
                              </td>
                              <td class="actions" data-th="">
                                  <button class="btn btn-link remove-from-cart bigger a-color" style="color:gray;text-decoration:none" data-id="{{ $id }}">Rimuovi<i data-feather="trash-2"></i></button>
                              </td>
                          </tr>
                      </tbody>
                    </table>
                   @endif
                 </div>
                @endforeach
          </div>
          <br>
       </div>
    </div>
    <!--totale-->
    <div class="col-md-3">
      <!--calcolo spese di spedizione in base al subtotale-->
      @if ($subtotal < 50)
        @php
          $spedizione = 7.50;
          $total = $subtotal + $spedizione;
        @endphp
      @else
        @php
          $spedizione = 5.00;
          $total = $subtotal + $spedizione;
        @endphp
      @endif
      <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
        <h3>Totale</h3>
        <br>
        <table class="table table-md">
          <tbody>
            <tr>
              <th scope="row">Subtotale</th>
              <td class="subtotal">{{ number_format((float)$subtotal , 2, '.', '')}}€</td>
            </tr>
            <tr>
              <th scope="row">Spese di Spedizione</th>
              <td class="spedizione">{{ number_format((float)$spedizione , 2, '.', '')}}€</td>
            </tr>
            <tr>
              <th scope="row">Totale (IVA inclusa)</th>
              <td class="total">{{ number_format((float)$total , 2, '.', '')}}€</td>
            </tr>
          </tbody>
        </table>
        <a class="btn btn-primary bigger" href="{{ URL::action('PurchaseController@editAddress') }}" role="button">Acquista <i data-feather="arrow-right"></i></a>
      </div>
    </div>
  </div>


  <!--se non c'è nessun articolo nel carrello-->
  @else
  <div class="row">
      <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
        <div class="container">
          <h3>Il tuo carrello</h3>
          <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
            <div class="col-md-5 p-lg-5 mx-auto my-5">
              <br>
              <br>
              <h4>Ops, sembra che il tuo carrello sia vuoto!</h4>
              <br>
              <p><a class="btn btn-primary bigger" href="{{ URL::action('BookController@index') }}" role="button">Inizia ad acquistare <i data-feather="plus"></i></a></p>
            </div>
          </div>
        </div>
        <br>
      </div>
    </div>
  @endif
</div>

@endsection
