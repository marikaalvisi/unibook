<script src="https://unpkg.com/feather-icons"></script>

<style media="screen">
  /*pagination*/

  .pagination a {
    font-weight:bold;
    color: gray;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
  }

</style>


@if ($paginator->hasPages())
<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <!--disabilitato-->
            <li>
                <a href="#" class="bigger" aria-label="Previous" style="border:none">
                    <span aria-hidden="true"><i data-feather="chevrons-left"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
@else
            <!--attivo-->
            <li><a class="bigger" href="{{ $paginator->previousPageUrl() }}" rel="prev" style="border:none; color:#ff8c00"><i data-feather="chevrons-left"></i></a></li>
@endif

{{-- Pagination Elements --}}
@foreach ($elements as $element)
{{-- "Three Dots" Separator --}}
    @if (is_string($element))
            <!--disabilitato-->
            <li>{{ $element }}</li>
    @endif

    {{-- Array Of Links --}}
    @if (is_array($element))
        @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                        <!--attivo-->
                        <li>
                            <a class="bigger" href="#" style="border:none; background-color:#ff8c00; color:white">{{ $page }}<span class="sr-only">(current)</span></a>
                        </li>
                @else
                        <!--non attivo-->
                        <li>
                            <a class="bigger" href="{{ $url }}" style="border:none; color:#ff8c00">{{ $page }}</a>
                        </li>
                @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
                <!--attivo-->
                <li><a class="bigger" href="{{ $paginator->nextPageUrl() }}" rel="next" style="border:none; color:#ff8c00"><i data-feather="chevrons-right"></i></a></li>
        @else
                <!--disabilitato-->
                <li>
                    <a class="bigger" href="#" aria-label="Next" style="border:none">
                        <span aria-hidden="true"><i data-feather="chevrons-right"></i></span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
        @endif
    </ul>
  </nav>
    @endif


    <script>
      feather.replace()
    </script>
