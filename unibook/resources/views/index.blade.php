@extends ('layout.app')
@section('content')
    <header>
      <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light" style="max-width: 100%">
        <div class="col-md-5 p-lg-5 mx-auto my-5 bigger">
          <img src="logo6.png" class="display-4" style="width:200;height:70">
          <p class="lead font-weight-normal">Hai bisogno di un testo universitario? Scegli tra la versione nuova e usata!<br>Perché spendere di più?</p>
        </div>
        <p class="bigger a-color"><span onclick="scroll_to('arrivo')"><i data-feather="arrow-down" width="50" height="50"></i></span></p>
      </div>
    </header>

    <div id="arrivo"></div>

    <div class="row">
      <div class="col">
        <div class="shadow p-3 mb-5 bg-white rounded">

          <div class="container" style="background-color:white">
            <br>
            <div class="row featurette">
              <div class="col-md-7">
                <a style="color:#ff8c00; text-decoration:none" href="{{ URL::action('BookController@index') }}"><h2 class="featurette-heading bigger">Vuoi comprare un libro usato? <span style="color:#ff8c00" > Stay cool!</span></h2></a>
                <p class="lead">Ci occuperemo noi di tutto: ritiro, controllo e consegna. Chi altro ti offre un servizio così?</p>
              </div>
              <!--<div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
              </div>-->
            </div>
            <br>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="background-color:white">
            <br>
            <div class="row featurette">
              <div class="col-md-7 push-md-5">
                <a style="color:#ff8c00; text-decoration:none" href="{{ URL::asset('/myaccount/mybooks') }}"><h2 class="featurette-heading bigger">Vuoi vendere un libro? <span style="color:#ff8c00">Do it!</span></h2></a>
                <p class="lead">Quando verrà acquistato, il corriere ti consegnerà i tuoi soldi al momento del ritiro!<br>Noi ci terremo solo il 10%!</p>
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>

    <br>

    <div class="row">
      <div class="col-lg-4">
        <div class="shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="background-color:white">
            <br>
            <img class="rounded-circle" src="shopping-cart.png" alt="Generic placeholder image" width="140" height="140">
            <h2>Acquisto</h2>
            <p>Acquista i tuoi testi universitari, puoi scegliere tra la versione nuova e le versioni usate messe in vendita da altri utenti come te</p>
            <p><a class="btn btn-secondary" href="{{ URL::asset('books') }}" role="button">Scopri <i data-feather="arrow-right"></i></a></p>
            <br>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="background-color:white">
            <br>
            <img class="rounded-circle" src="delivery-cart.png" alt="Generic placeholder image" width="140" height="140">
            <h2>Ritiro</h2>
            <p>Se il testo universitario che hai deciso di acquistare è usato, andremo personalmente a ritirarlo dal venditore, verificando che le sue condizioni rispettino quelle da lui indicate.</p>
            <p><a class="btn btn-secondary" href="{{ URL::asset('infodelivery') }}" role="button">Scopri <i data-feather="arrow-right"></i></a></p>
            <br>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="background-color:white">
            <br>
            <img class="rounded-circle" src="delivery-truck.png" alt="Generic placeholder image" width="140" height="140">
            <h2>Consegna</h2>
            <p>Consegnamo il pacco direttamente a casa tua, guarda nella sezione "info spedizione" per conoscere le nostre tariffe di consegna in tutta Italia</p>
            <p><a class="btn btn-secondary" href="{{ URL::asset('infodelivery') }}" role="button">Scopri <i data-feather="arrow-right"></i></a></p>
            <br>
          </div>
        </div>
      </div>
    </div>

    <br>
    <br>

    <!--ultimi libri nuovi aggiunti-->
    <div class="shadow p-3 mb-5 bg-white rounded">
      <div class="container" style="background-color:white;">
        <br>
        <div align="center">
          <h4>Ultimi libri aggiunti</h4>
        </div>
        <br>
        @if(count($books)>0)
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="resize80">
              <div class="carousel-inner" role="listbox">
              <br>
              <div class="carousel-item active">
                <div class="card-deck resize90">
                  @php
                   $i = 0;
                  @endphp
                  @foreach ($books as $book)
                  <div class="bigger card" style="border:hidden">
                    <a href="{{ URL::action('BookController@show', $book->id) }}" style="text-decoration:none; color:black">
                      <img class="card-img-top" src="{{ url('images/'.$book->image) }}">
                      <div class="card-block">
                        <h6 class="card-title truncate">{{$book->title}}</h6>
                        <p class="card-text truncate">{{$book->author}}</p>
                        <p class="card-text"><small class="text-muted">{{$book->course}}</small></p>
                        <h6 class="card-text" align="right">{{ number_format((float)$book->price , 2, '.', '')}}€</h6>
                        <br>
                      </div>
                    </a>
                  </div>
                  @php
                  $i = $i+1;
                  @endphp
                  @if ($i % 4 == 0 && count($books) > $i)
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="card-deck resize90">
                  @endif
                  @endforeach
                  @if($i%4 != 0)
                    @if(($i+1)%4 == 0)
                      <div class="card" style="border:hidden"></div>
                    @endif

                    @if(($i+2)%4 == 0)
                      <div class="card" style="border:hidden"></div>
                      <div class="card" style="border:hidden"></div>
                    @endif

                    @if(($i+3)%4 == 0)
                      <div class="card" style="border:hidden"></div>
                      <div class="card" style="border:hidden"></div>
                      <div class="card" style="border:hidden"></div>
                    @endif
                  @endif
                </div>
              </div>
            </div>
           </div>

           @if($i> 4)
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="container carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          @endif
        </div>
      @else
        <h6 align="center">Ops, colpa nostra!<br>non abbiamo ancora messo nessun testo universitario in vendita!<br></h6>
      @endif
    </div>
    <br>
  </div>

  <br>
  <br>

  <!--ultimi libri usati aggiunti-->
  <div class="shadow p-3 mb-5 bg-white rounded">
    <div class="container" style="background-color:white;">
    <br>
    <div align="center">
      <h4>I libri degli altri utenti</h4>
    </div>
    <br>
    @if(count($usedBooks) > 0)
      <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
        <div class="resize80">
          <div class="carousel-inner" role="listbox">
            <br>
            <div class="carousel-item active">
              <div class="card-deck resize90">
                @php
                 $i = 0;
                @endphp
                @foreach ($usedBooks as $book)
                <div class="bigger card" style="border:hidden">
                  <a href="{{ URL::action('BookController@show', $book->id) }}" style="text-decoration:none; color:black">
                    <img class="card-img-top" src="{{ url('images/'.$book->image) }}">
                    <div class="card-block">
                      <h6 class="card-title truncate">{{$book->title}}</h6>
                      <p class="card-text truncate">{{$book->author}}</p>
                      <p class="card-text"><small class="text-muted">{{$book->course}}</small></p>
                      <h6 class="card-text" align="right">{{ number_format((float)$book->price , 2, '.', '')}}€</h6>
                      <br>
                    </div>
                  </a>
                </div>
                @php
                $i = $i+1;
                @endphp
                @if ($i == 4 && count($usedBooks) > $i)
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="card-deck resize90">
                @endif
                @endforeach
                @if($i%4 != 0)
                  @if(($i+1)%4 == 0)
                    <div class="card" style="border:hidden"></div>
                  @endif

                  @if(($i+2)%4 == 0)
                    <div class="card" style="border:hidden"></div>
                    <div class="card" style="border:hidden"></div>
                  @endif

                  @if(($i+3)%4 == 0)
                    <div class="card" style="border:hidden"></div>
                    <div class="card" style="border:hidden"></div>
                    <div class="card" style="border:hidden"></div>
                  @endif
                @endif
              </div>
            </div>
            <br>
          </div>
         </div>

         @if($i> 4)
          <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>

          <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
              <span class="container carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          @endif

        </div>
      @else
        <h6 align="center">Ops, sembra che nessun utente abbia ancora messo in vendita un testo universitario!</h6>
      @endif
      <br>
    </div>
  </div>
@endsection
