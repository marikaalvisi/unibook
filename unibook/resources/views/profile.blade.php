@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div class="container">

    <br>
    <div class="row">
      <div class="col-sm-3">
        <h6>Il mio account</h6>
        <div class="container">
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/profile') }}">Il mio profilo</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/myorders') }}">I miei ordini</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/mybooks') }}">I miei libri</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/credentials') }}">Opzioni di accesso</a>
        </div>
      </div>

      <div class="col-sm-9">
        <!--breadcrumb-->
        <a class="breadcrumb-item" style="color:black" href="{{ URL::asset('/myaccount') }}">Il mio account</a>
        <span class="breadcrumb-item active">Il mio profilo</span>

        <br>
        <br>

        <h3>Il mio profilo</h3>
        <p>Da qui puoi gestire le tue informazioni di contatto.</p>

        <br>


        <div class="row">

          <div class="col-md-auto">
            <i data-feather="user" width="100" height="100"></i>
          </div>

          <div class="col-md-auto">
            <div class="row">
              <div class="table-responsive-sm">
                <table class="table table-borderless">
                  <thead>
                    <tr>
                      <th scope="col">Username</th>
                      <th scope="col">Nome</th>
                      <th scope="col">Cognome</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{{ Auth::user()->username }}</td>
                      <td>{{ Auth::user()->name }}</td>
                      <td>{{ Auth::user()->surname }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="row grow">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="text-decoration:none">
                Modifica <i data-feather="arrow-right"></i>
              </button>
            </div>



          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modifica i dati del tuo account </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="{{ URL::action('UserController@update', Auth::user()->id)}}" method="POST" id="form">
                {{ csrf_field() }}

                <!--nome-->
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{Auth::user()->username}}">
                </div>

                <!--nome-->
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->name}}">
                </div>

                <!--cognome-->
                <div class="form-group">
                    <label for="surname">Cognome</label>
                    <input type="text" class="form-control" id="surname" name="surname" value="{{Auth::user()->surname}}">
                </div>

                <div class="grow">
                  <button type="submit" class="btn btn-primary">Aggiorna <i data-feather="refresh-cw"></i></button>
                </div>

                <br>

                <div class="grow">
                  <a  class="btn btn-secondary" href="{{ URL::asset('myaccount/profile') }}" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
                </div>

            </form>
          </div>
        </div>
      </div>
    </div>


  </div>
  <br>
</div>

@endsection
