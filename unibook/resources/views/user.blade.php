@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 bg-white rounded" style="background-color:white">
  <div class="row">
    <div class="col-md-auto">
      <i data-feather="user" width="100" height="100"></i>
    </div>
    <div class="col-md-auto">
      <div class="row">
        <div class="col-md-3">
          <p style=color:#ff8c00>Username</p>
        </div>
        <div class="col-md-3">
          <p style=color:#ff8c00>Nome</p>
        </div>
        <div class="col-md-3">
          <p style=color:#ff8c00>Cognome</p>
        </div>
        <div class="col-md-3">
          <p style=color:#ff8c00>Email</p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-3">
          <p>{{ $user->username }}</p>
        </div>
        <div class="col-md-3">
          <p>{{ $user->name }}</p>
        </div>
        <div class="col-md-3">
          <p>{{ $user->surname }}</p>
        </div>
        <div class="col-md-3">
          <p>{{ $user->email }}</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container shadow p-3 mb-5 bg-white rounded" style="background-color:white">
  <br>
  <h3 style="color:#ff8c00">I testi universitari usati di {{$user->username}}</h3>
  @if (isset($userBooks) && count($userBooks) > 0)
   @foreach($userBooks as $row)
     <hr>
     <div class="container box">
        <div class="row">
           <div class="col-sm-4" >
           <!--foto libro-->
             <img src="{{ url('images/'.$row->image) }}" class="rounded float-left" width="200" height="275" alt="{{$row->title}}">
           </div>

           <div class="col-sm-6">
             <!--info libro-->
             <table class="table table-sm">
               <tbody>
                 <tr>
                   <th scope="row">Titolo</th>
                   <td>{{$row->title}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Autore</th>
                   <td>{{$row->author}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Editore</th>
                   <td>{{$row->editor}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Edizione</th>
                   <td>{{$row->edition}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Pagine</th>
                   <td>{{$row->pages}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Prezzo</th>
                   <td>{{number_format((float)$row->price, 2, '.', '')}}€</td>
                 </tr>
                 <tr>
                   <th scope="row">Descrizione</th>
                   <td>{{$row->description}}</td>
                 </tr>
               </tbody>
             </table>
             <br>
             <!--pulsanti-->
             <div class="row">
               @if($row->availability > 0)
               <div class="grow">
                 <button class="btn btn-primary add-to-cart" data-id="{{ $row->id }}">Metti nel carrello <i data-feather="plus"></i></button>
               </div>
               <div class="bigger">
                 <button class="btn btn-link a-color add-to-wishlist" data-id="{{ $row->id }}" style="color: #ff8c00"><i data-feather="heart"></i></button>
               </div>
               @else
                <h6 style="color:#ff8c00">Venduto!</h6>
               @endif
           </div>
         </div>
       </div>
     </div>
    <br>
   @endforeach

   @else
   <tr>
    <td align="center" colspan="5">Nessun libro trovato</td>
   </tr>

  @endif
</div>

@endsection
