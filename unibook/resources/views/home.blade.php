@extends('layout.app')

@section('content')
<br>
<br>

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <div class="container">
    <h4 class="display-5 text-uppercase font-weight-bold">Ciao, {{ Auth::user()->username }}</h4>
    <p class="lead">Hai appena effettuato l'accesso.</p>
    <hr class="my-4">
    <p class="lead">
      <div class="grow">
         <a class="btn btn-primary" href="{{ URL::asset('/') }}" role="button" style="text-decoration:none"> Vai alla homepage <i data-feather="arrow-right"></i></a>
       </div>
    </p>
  </div>
</div>
@endsection
