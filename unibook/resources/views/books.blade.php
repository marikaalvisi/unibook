@extends ('layout.app')
@section('content')

  <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
     <br>
     <h4 align="center">Cerca qui i tuoi testi universitari</h4><br />
     <div class="panel panel-default">
      <div class="panel-body">
       <div class="form-group">
        <input type="text" name="search" id="search" class="form-control" placeholder="Cerca per titolo">
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="course">Filtra per corso</label>
              <select class="form-control" name="course" id="course">
                <option>Qualsiasi</option>
                @foreach($courses as $course)
                  <option>{{$course->course}}</option>
                @endforeach
                <option>Altro</option>
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="condition">Condizioni</label>
              <select class="form-control" name="condition" id="condition">
                <option>Qualsiasi</option>
                <option>Nuovo</option>
                <option>Usato</option>
              </select>
            </div>
          </div>
        </div>

        <div class="container">
          <br>
          <div id="result">
          @include('pagination_data')
          <!--ajax, vedere BookController@action-->
          </div>
          <br>
        </div>
        <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
      </div>
    </div>
  </div>

@endsection
