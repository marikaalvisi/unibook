@extends('layout.app')

@section('content')

<div class="container shadow rounded"style="background-color:white">
  <nav class="breadcrumb" style="background-color:white;">
    <span class="breadcrumb-item" style="color:gray">Indirizzo</span>
    <span class="breadcrumb-item" style="color:gray; text-decoration:none" href="">Pagamento</span>
    <span class="breadcrumb-item active" style="color:#ff8c00; text-decoration:none" href="">Riepilogo</span>
  </nav>
</div>

  <div class="row">
    <div class="col-md-9">
      <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
        <div class="container">
          <div align="right">
            <a class="btn btn-primary bigger" href="{{ URL::action('PurchaseController@confirm') }}" role="button">Conferma l'ordine <i data-feather="check"></i></a>
          </div>
          <h3>Controlla i tuoi dati e conferma l'ordine</h3>
          <br>
          <h5>Articoli</h5>
          @php
            $subtotal = 0;
          @endphp
          @if(session('cart') != null)
          @foreach(session('cart') as $id => $details)
                @php
                  $subtotal += $details['price'] * $details['quantity']
                @endphp
                <hr>
                <div class="row">
                  <div class="col-md-3">
                    <img src="{{url('images/'.$details['image'])}}" class="rounded float-left" width="140" height="200">
                  </div>
                  <div class="col-md-9">
                    <h6>{{$details['title']}}</h6>
                    <p style="color:gray">{{$details['author']}}</p>
                    <br>
                    <h6>{{ number_format((float)$details['price'] * $details['quantity'], 2, '.', '')}}€</h6>
                    <br>
                    <h6>Quantità: {{$details['quantity']}}</h6>
                    <br>
                  </div>
                </div>
          @endforeach
          @endif
        </div>
        <br>
        <hr>
        <div align="right">
          <a class="btn btn-primary bigger" href="{{ URL::action('PurchaseController@confirm') }}" role="button">Conferma l'ordine <i data-feather="check"></i></a>
        </div>
      </div>
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
            <div class="container">
              <!--calcolo spese di spedizione in base al subtotale-->
              @if ($subtotal < 50)
                @php
                  $spedizione = 7.50;
                  $total = $subtotal + $spedizione;
                @endphp
              @else
                @php
                  $spedizione = 5.00;
                  $total = $subtotal + $spedizione;
                @endphp
              @endif
              <h5>Totale</h5>
              <table class="table table-sm">
                <tbody>
                  <tr>
                    <th scope="row">Subtotale</th>
                    <td>{{ number_format((float)$subtotal, 2, '.', '')}}€</td>
                  </tr>
                  <tr>
                    <th scope="row">Spese di Spedizione</th>
                    <td>{{ number_format((float)$spedizione, 2, '.', '')}}€</td>
                  </tr>
                  <tr>
                    <th scope="row">Totale (IVA inclusa)</th>
                    <td>{{ number_format((float)$total, 2, '.', '')}}€</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
            <div class="container">
              <!--indirizzo di spedizione-->
              <!--remember tempi di spedizione-->
              <h5>Indirizzo di spedizione</h5>
              @php
                $address = session('address');
              @endphp
              <p>{{$address['name']}} {{$address['surname']}}<br>{{$address['address']}}<br>{{$address['CAP']}}<br>{{$address['city']}}<br></p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
            <div class="container">
              <h5>Modalità di pagamento</h5>
              @php
                $payment = session('payment');
              @endphp
              <p>{{$payment['type']}}</p>
              <p style="color:gray">Ricorda: prepara l’importo esatto: il vettore non può dare il resto!</p>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
