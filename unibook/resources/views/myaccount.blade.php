@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div class="container">

    <br>
    <div class="row">
      <div class="col-sm-3">
        <h6>Il mio account</h6>
        <div class="container">
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/profile') }}">Il mio profilo</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/myorders') }}">I miei ordini</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/mybooks') }}">I miei libri</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/credentials') }}">Opzioni di accesso</a>
        </div>
      </div>

      <div class="col-sm-9">
        <h3>Ciao, {{ Auth::user()->username }}!</h3>
        <p>Da qui puoi gestire i tuoi ordini, libri in vendita e dati di contatto.</p>

      </div>
    </div>
  </div>
  <br>
</div>

@endsection
