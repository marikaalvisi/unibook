@if (isset($data) && count($data) > 0)
  @foreach($data as $order)
    <div class="container">
           <h6 style="color:#ff8c00">Ordine #{{$order->id}}</h6>
           <br>
           <div class="container">

             <form action="{{ URL::action('PurchaseController@updateOrder', $order->id) }}" method="POST">
               {{ csrf_field() }}

               @if($order->order_status == "in elaborazione")

                  <div class="row">
                    <div class="col">
                      <label class="radio-style"><h6>In elaborazione</h6>
                        <input  type="radio" name="status" id="in elaborazione" value="in elaborazione" checked>
                        <span class="checkmark"></span>
                      </label>
                    </div>

                    <div class="col">
                      <label class="radio-style"><h6>In spedizione</h6>
                        <input type="radio" name="status" id="in spedizione" value="in spedizione">
                        <span class="checkmark"></span>
                      </label>
                    </div>

                    <div class="col">
                      <label class="radio-style"><h6>Consegnato</h6>
                        <input type="radio" name="status" id="consegnato" value="consegnato">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
               @endif

               @if($order->order_status == "in spedizione")
                 <div class="row">
                   <div class="col">
                     <label class="radio-style"><h6>In elaborazione</h6>
                       <input  type="radio" name="status" id="in elaborazione" value="in elaborazione">
                       <span class="checkmark"></span>
                     </label>
                   </div>

                   <div class="col">
                     <label class="radio-style"><h6>In spedizione</h6>
                       <input type="radio" name="status" id="in spedizione" value="in spedizione" checked>
                       <span class="checkmark"></span>
                     </label>
                   </div>

                   <div class="col">
                     <label class="radio-style"><h6>Consegnato</h6>
                       <input type="radio" name="status" id="consegnato" value="consegnato">
                       <span class="checkmark"></span>
                     </label>
                   </div>
                 </div>
               @endif

               @if($order->order_status == "consegnato")
                <h6>Ordine consegnato</h6>
                <h6 style="color:gray">Non è più possibile modificare il suo stato!</h6>
               @endif

             <br>

             @if ($order->status != "consegnato")
             <div class="grow">
               <button type="submit" class="btn btn-primary">Modifica lo stato dell'ordine #{{$order->id}}<i data-feather="arrow-right"></i></button>
             </div>
             @endif
           </form>

           </div>

           <hr>
            <br>
          </div>
          @endforeach

          {{ $data->links( "pagination::pagination") }}

       @else
       <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
           <br>
           <h4>Ops, nessun ordine trovato!</h4>
           <br>
       </div>
       @endif
