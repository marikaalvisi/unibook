@if (isset($data) && count($data) > 0)
  <div class="flex-container">

    @foreach( $data as $book )
      <div class="bigger card" style="border:none">
        <a href="{{ URL::action('BookController@show', $book->id) }}" style="text-decoration:none; color:black"><img class="card-img-top" src="{{ url('images/'.$book->image) }}" alt="Card image cap">
          <div class="card-block">
            <h6 class="card-title">{{$book->title}}</h6>
            <p class="card-text">{{$book->author}}</p>
            @if($book->condition == "new")
              <p class="card-text" style="color:green">Versione nuova</p>
            @endif
            @if($book->condition == "used")
              <p class="card-text" style="color:#ff8c00">Versione usata</p>
            @endif
            <p class="card-text"><small class="text-muted">{{$book->course}}</small></p>
            <h6 class="card-text" align=right>{{ number_format((float)$book->price , 2, '.', '')}}€</h6>
          </div>
        </a>
      </div>
    @endforeach
  </div>

<br>
{{ $data->links( "pagination::pagination") }}


@else
  <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
      <br>
      <h4>Ops, nessun risultato trovato!</h4>
      <br>
  </div>
@endif
