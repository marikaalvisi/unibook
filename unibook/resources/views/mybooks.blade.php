@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div class="container">

    <br>
    <div class="row">
      <div class="col-sm-3">
        <h6>Il mio account</h6>
        <div class="container">
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/profile') }}">Il mio profilo</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/myorders') }}">I miei ordini</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/mybooks') }}">I miei libri</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/credentials') }}">Opzioni di accesso</a>
        </div>
      </div>

      <div class="col-sm-9">
        <!--breadcrumb-->
        <a class="breadcrumb-item" style="color:black" href="{{ URL::asset('/myaccount') }}">Il mio account</a>
        <span class="breadcrumb-item active">I miei libri</span>

        <br>
        <br>
        <h3>I miei libri</h3>
        <p>Da qui puoi mettere in vendita i tuoi testi universitari usati e gestire quelli già in vendita.</p>

        <br>

        <h6 style="color:gray">Ricorda</h6>
        <p style="color:gray">Ci occuperemo noi del ritiro a casa tua e della consegna all'acquirente.<br>
        Tu preoccupati solo che il libro rispetti le condizioni che indicherai nella descrizione!<br><br>
        Quando uno dei libri che hai messo in vendita verrà venduto, sarà il corriere a consegnarti i soldi al momento del ritiro.<br>Noi terremo solo il 10%!</p>

        <br>

        <div class="grow">
          <button type="button" class="btn btn-primary" style="text-decoration:none" data-toggle="modal" data-target="#exampleModalScrollable">
            Aggiungi libro <i data-feather="plus"></i>
          </button>
        </div>

        <br>

        <!--dashboard utente-->
        <div class="panel panel-default">
         <div class="panel-body">
          <div class="form-group">
           <input type="text" name="search3" id="search3" class="form-control" placeholder="Cerca tra i tuoi libri...">
          </div>
          <br>
          <div id='resultUser'>
            @include('pagination_user_dashboard')
            <!--ajax, vedere BookController@actionDashboardUser-->
          </div>
         </div>
        </div>



        <!-- Modal -->
        <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Nuovo libro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                @if (count($errors) > 0)
                  <script>
                    Swal.fire({
                      type: 'warning',
                      title: 'Oops...',
                      text: 'Sembra che tu non abbia inserito correttamente i campi!',
                      showConfirmButton: false,
                      timer: 1500,
                    })
                  </script>
                @endif

                <form action="{{ URL::action('BookController@storeUser') }}" method="POST" id="form" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <!--titolo-->
                    <div class="form-group">
                        <label for="title">Titolo <strong style="color:#ff8c00">*</strong></label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>

                    <!--autore-->
                    <div class="form-group">
                        <label for="author">Autore <strong style="color:#ff8c00">*</strong></label>
                        <input type="text" class="form-control" id="author" name="author">
                    </div>

                    <!--corso-->
                    <div class="form-group">
                      <label for="course">Corso di studi</label>
                      <select class="form-control" name="course" id="course">
                        @foreach($courses as $course)
                          <option>{{$course->course}}</option>
                        @endforeach
                        <option>Altro</option>
                      </select>
                    </div>

                    <!--immagine-->
                    <div class="form-group">
                      <label for="image">Immagine</label>
                      <input type="file" class="form-control-file" id="image" name="image">
                    </div>

                    <!--Descrizione-->
                    <div class="form-group">
                      <label for="description">Descrizione</label>
                      <textarea class="form-control" id="description" name="description"></textarea>
                    </div>

                    <!--prezzo-->
                    <div class="form-group">
                        <label for="author">Prezzo <span style="color:gray">(Prezzo massimo: 999.99€)</span><strong style="color:#ff8c00">*</strong></label>
                        <input type="text" class="form-control" id="price" name="price">
                    </div>

                    <!--editore-->
                    <div class="form-group">
                        <label for="author">Editore <strong style="color:#ff8c00">*</strong></label>
                        <input type="text" class="form-control" id="editor" name="editor">
                    </div>

                    <!--edizione-->
                    <div class="form-group">
                        <label for="author">Edizione <strong style="color:#ff8c00">*</strong></label>
                        <input type="text" class="form-control" id="edition" name="edition">
                    </div>

                    <!--pagine-->
                    <div class="form-group">
                        <label for="author">Pagine <strong style="color:#ff8c00">*</strong></label>
                        <input type="text" class="form-control" id="pages" name="pages">
                    </div>

                    <div class="grow">
                      <button type="submit" class="btn btn-primary">Aggiungi <i data-feather="plus"></i></button>
                    </div>

                    <br>

                    <div class="grow">
                      <a href="{{ URL::action('BookController@dashboard') }}" class="btn btn-secondary" data-dismiss="modal" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
                    </div>

                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <br>
</div>

@endsection
