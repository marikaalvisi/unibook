@extends ('layout.app')
@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <div class="row">
    <div class="col-sm">
      <h4>Cosa dicono di noi i nostri clienti</h4>
    </div>
    @if(Auth::check())
      <div class="col-sm">
        <div align="right">
          <button type="button" class="btn btn-link bigger a-color" style="color:gray;text-decoration:none"  data-toggle="modal" data-target="#exampleModalScrollable">Aggiungi una recensione <i data-feather="plus"></i></button>
        </div>
      </div>
    @endif
  </div>
  <div class="container">
    @if (count($reviews) > 0)
      @foreach($reviews as $review)
        <hr>
          <h6 style="color:gray">{{ $review->user->username }} <span style="color:#ff8c00">{{ $review->rate}}/5</span></h6>
        <p>{{ $review->review }}</p>
      @endforeach
    @else
      <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
          <br>
          <br>
          <h4>Ops, nessuno ha ancora lasciato una recensione,<br>che ne dici di essere tu a lasciare la prima?</h4>
          <br>
        </div>
      </div>
    @endif
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Aggiungi una recensione</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @if (count($errors) > 0)
          <script>
            Swal.fire({
              type: 'warning',
              title: 'Oops...',
              text: 'Sembra che tu non abbia inserito correttamente i campi!',
              showConfirmButton: false,
              timer: 1500,
            })
          </script>
        @endif
        <form action="{{ URL::action('ReviewController@store') }}" method="POST" id="form">
            {{ csrf_field() }}

            <div class="form-group">
              <label for="review">Recensione <strong style="color:#ff8c00">*</strong></label>
              <textarea class="form-control" id="review" name="review" rows="3"></textarea>
            </div>

            <label>Valutazione <strong style="color:#ff8c00">*</strong></label>

            <div class="form-group row">
              <div class="col-sm-3">
                <input type="number" value="5" id="rate" name="rate" class="form-control" min="0" max="5" step="0.5">
              </div>
              <label for="rate" class="col-sm-1 col-form-label"><strong style="color:gray">/5</strong></label>
            </div>

            <div class="grow">
              <button type="submit" class="btn btn-primary">Invia <i data-feather="arrow-right"></i></button>
            </div>

            <br>

            <div class="grow">
              <a  class="btn btn-secondary" href="{{ URL::asset('/aboutus') }}" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
            </div>

        </form>
      </div>
    </div>
  </div>
</div>

@endsection
