@extends('layout.app')

@section('content')

<div class="container" style="background-color:white">
  <br>
  <div class="container">
      <h3>Modifica il tuo indirizzo email</h3>
      <p>Ti invieremo tutte le comunicazioni importanti all’indirizzo email aggiornato</p>

      <br>
      
      <form action="{{ URL::action('UserController@update', $user->id) }}" method="POST">

          {{ csrf_field() }}

          <h5>Email corrente</h5>
          <p>{{$user->email}}</p>

          <div class="form-group">
              <label for="email">Nuova email</label>
              <input type="text" class="form-control" id="email" name="email">
          </div>

          <div class="form-group">
              <label for="email">Conferma la nuova email</label>
              <input type="text" class="form-control" id="email" name="email">
          </div>

          <div class="form-group">
              <label for="password">Password</label>
              <input type="text" class="form-control" id="password" name="password">
          </div>

          <div class="grow">
            <button type="submit" class="btn btn-primary">Aggiorna</button>
          </div>

          <br>

          <div class="grow">
            <a href="{{ URL::asset('myaccount/credentials') }}" style="color:black; text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
          </div>

        </form>
    </div>
    <br>
</div>


@endsection
