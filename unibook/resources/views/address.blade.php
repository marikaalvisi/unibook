@extends('layout.app')

@section('content')


<div class="container shadow rounded"style="background-color:white">
  <nav class="breadcrumb" style="background-color:white;">
    <span class="breadcrumb-item active" style="color:#ff8c00">Indirizzo</span>
    <span class="breadcrumb-item" style="color:gray; text-decoration:none" href="">Pagamento</span>
    <span class="breadcrumb-item" style="color:gray; text-decoration:none" href="">Riepilogo</span>
  </nav>
</div>

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <div class="container">
    <h3>Indirizzo di consegna</h3>

    @if (count($errors) > 0)
      <script>
        Swal.fire({
          type: 'warning',
          title: 'Oops...',
          text: 'Sembra che tu non abbia inserito correttamente i campi!',
          showConfirmButton: false,
          timer: 2000,
        })
      </script>
    @endif


    <form action="{{ URL::action('PurchaseController@addAddress') }}" method="POST">

        {{ csrf_field() }}

        <!--Nome-->
        @if(Auth::user()->name == "sconosciuto")
          <div class="form-group">
              <label for="name">Nome <strong style="color:#ff8c00">*</strong></label>
              <input type="text" class="form-control" id="name" name="name">
          </div>
        @else
          <div class="form-group">
              <label for="name">Nome <strong style="color:#ff8c00">*</strong></label>
              <input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->name}}">
          </div>
        @endif

        <!--Cognome-->
        @if(Auth::user()->surname == "sconosciuto")
          <div class="form-group">
              <label for="surname">Cognome <strong style="color:#ff8c00">*</strong></label>
              <input type="text" class="form-control" id="surname" name="surname">
          </div>
        @else
          <div class="form-group">
              <label for="surname">Cognome <strong style="color:#ff8c00">*</strong></label>
              <input type="text" class="form-control" id="surname" name="surname" value="{{Auth::user()->surname}}">
          </div>
        @endif


        <!--Via e numero civico-->
        <div class="form-group">
            <label for="address">Via e numero civico <strong style="color:#ff8c00">*</strong></label>
            <input type="text" class="form-control" id="address" name="address">
        </div>

        <!--CAP-->
        <div class="form-group">
          <label for="CAP">CAP <strong style="color:#ff8c00">*</strong></label>
          <input type="text" class="form-control" id="CAP" name="CAP">
        </div>

        <!--Città-->
        <div class="form-group">
            <label for="city">Città <strong style="color:#ff8c00">*</strong></label>
            <input type="text" class="form-control" id="city" name="city">
        </div>

        <div class="grow">
          <button type="submit" class="btn btn-primary">Usa come indirizzo di consegna <i data-feather="arrow-right"></i></button>
        </div>

        <br>
        <div class="grow">
          <a href="{{ URL::asset('/cart') }}" class="btn btn-secondary" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
        </div>
      </form>
  </div>
</div>

@endsection
