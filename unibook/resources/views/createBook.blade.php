@extends ('layout.app')
@section('content')

  <br>
  <div class="container shadow p-3 mb-5 rounded">
    <div class="row">
        <h1>Inserisci i dati di un nuovo libro</h1>

        <div class="col-sm-6">
            <form action="{{ URL::action('BookController@store') }}" method="POST">

                {{ csrf_field() }}

                <!--titolo-->
                <div class="form-group">
                    <label for="title">Titolo <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>

                <!--autore-->
                <div class="form-group">
                    <label for="author">Autore <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="author" name="author">
                </div>

                <!--corso-->
                <div class="form-group">
                    <label for="author">Corso di studi <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="course" name="course">
                </div>

                <!--immagine-->
                <div class="form-group">
                  <label for="image">Immagine</label>
                  <input type="file" class="form-control-file" id="image" name="image">
                </div>

                <!--Descrizione-->
                <div class="form-group">
                  <label for="description">Descrizione</label>
                  <textarea class="form-control" id="description" name="description"></textarea>
                </div>

                <!--prezzo-->
                <div class="form-group">
                    <label for="author">Prezzo <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="price" name="price">
                </div>

                <!--editore-->
                <div class="form-group">
                    <label for="author">Editore <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="editor" name="editor">
                </div>

                <!--edizione-->
                <div class="form-group">
                    <label for="author">Edizione <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="edition" name="edition">
                </div>

                <!--pagine-->
                <div class="form-group">
                    <label for="author">Pagine <strong style="color:red">*</strong></label>
                    <input type="text" class="form-control" id="pages" name="pages">
                </div>

                <!--condizioni-->
                  <!--<label for="condition">Condizioni</label>
                  <br>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="condition" id="condition" checked="checked" value="new">
                    <label class="form-check-label" for="condition">Nuovo</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="condition" id="condition" value="used">
                    <label class="form-check-label" for="condition">Usato</label>
                  </div>-->

                <!--Disponibilità-->
                @if (Auth::user()->role == 'admin')
                  <div class="form-group">
                      <label for="author">Pezzi attualmente disponibili</label>
                      <input type="text" class="form-control" id="availability" name="availability">
                  </div>
                @endif

                <div class="grow">
                  <button type="submit" class="btn btn-primary">Invia</button>
                </div>

                <br>

                <div class="grow">
                  <a href="{{ URL::action('BookController@dashboard') }}" style="color:black; text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
                </div>

            </form>
        </div>
    </div>
    <br>
  </div>

@endsection
