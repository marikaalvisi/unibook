@if (isset($data) && count($data) > 0)
 @foreach($data as $row)
   @if($row->condition == "new")
     <div class="container box">
        <div class="row">
           <div class="col-md-4" >
           <!--foto libro-->
             <img src="{{ url('images/'.$row->image) }}" class="rounded float-left" width="200" height="275" alt="{{$row->title}}">
           </div>

           <div class="col-md-6">
             <!--info libro-->
             <table class="table table-sm">
               <tbody>
                 <tr>
                   <th scope="row">Titolo</th>
                   <td>{{$row->title}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Autore</th>
                   <td>{{$row->author}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Editore</th>
                   <td>{{$row->editor}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Edizione</th>
                   <td>{{$row->edition}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Pagine</th>
                   <td>{{$row->pages}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Disponibilità</th>
                   <td>{{$row->availability}}</td>
                 </tr>
                 <tr>
                   <th scope="row">Prezzo</th>
                   <td>{{ number_format((float)$row->price , 2, '.', '')}}€</td>
                 </tr>
               </tbody>
             </table>
             <br>
         </div>


         <div class="col-md-2">
           <!--crud-->

           <div class="row">
            <div class="bigger">
              <a href="/dashboardBooks/{{$row->id}}/edit" class="btn btn-link a-color" style="text-decoration:none;color:gray" align="right">Modifica <i data-feather="edit-3"></i></a>
            </div>
           </div>


           <div class="row">
            <div class="bigger">
              <a href="/dashboardBooks/{{$row->id}}/destroy" class="btn btn-link a-color" style="text-decoration:none; color:gray" align="right" id="remove">Elimina <i data-feather="trash-2"></i></a>
            </div>
           </div>

         </div>

       </div>
     </div>
    <br>
    <hr>
    <br>
    @endif
 @endforeach

 {{ $data->links( "pagination::pagination") }}


 @else
 <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
     <br>
     <h4>Ops, nessun libro trovato!</h4>
     <br>
 </div>
@endif
