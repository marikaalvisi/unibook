<html <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://unpkg.com/feather-icons"></script>


        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', '  Unibook') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://unpkg.com/feather-icons"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">


        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>


        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

  <!-- icona e ricerca -->
    <body style="background-color:#F8F9F9; height: 100%;">

      <style>

        html,
        body{
          height: 100%;
        }

        header{
          position:relative;
          height: 100%;
          background-size:cover;
        }

        .truncate {
          display:inline-block;
          width:180px;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
      }

        .btn.btn-primary {
            color: #fff;
            background-color: #ff8c00;
            border-color: #ff8c00;
          }

          .btn.btn-primary:focus {
            color: #fff;
            background-color: #ff8c00;
            border-color: #ff8c00;
            outline: none;
            box-shadow: none;
          }

          .btn.btn-primary:hover {
            color: #fff;
            background-color: #ff8c00;
            border-color: #ff8c00;
            outline: none;
            box-shadow: none;
          }

          .btn.btn-primary:active {
              color: #fff;
              background-color: #ff8c00;
              border-color: #ff8c00;
              outline: none;
            }

            .btn.btn-primary:active:hover {
              color: #fff;
              background-color: #ff8c00;
              border-color: #ff8c00;
              outline: none;
              box-shadow: none;
            }

            .btn-secondary:hover {
                background-color: #ff8c00;
                color: white;
              }

            .btn {
              border-radius: 0;
              border:none;
              text:white
            }

            .container {
                border-radius:.75rem
            }

            .bigger {
            -moz-transition: all 0.3s;
            -webkit-transition: all 0.3s;
            transition: all 0.3s;
            }

            .bigger:hover	{
            -moz-transform: scale(1.1);
            -webkit-transform: scale(1.1);
            transform: scale(1.1);
            }

          	.resize80{
          		align-content: center;
          		margin: auto;
          		max-width: device-width;
          		width: 80%;
          	}

            .resize90{
              align-content: center;
              margin: auto;
              max-width: device-width;
              width: 100%;
            }

            .nav-item a:hover {
                color: #ff8c00 !important;
                cursor: pointer;
            }

            .a-color:hover {
                color: #ff8c00 !important;
                cursor: pointer;
            }

            .carousel-control-prev-icon {
               background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E") !important;
              }
            .carousel-control-next-icon {
              background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E") !important;
            }

            .grow { transition: all .2s ease-in-out; }
            .grow:hover { transform: scale(1.01);opacity: 0.8;}

            .marketing .col-lg-4 {
            margin-bottom: 1.5rem;
            text-align: center;
            }
            .marketing h2 {
            font-weight: normal;
            }
            .marketing .col-lg-4 p {
            margin-right: .75rem;
            margin-left: .75rem;
            }

            .featurette-divider {
            margin: 5rem 0;
            background-color:#ff8c00;
            }

            .featurette-heading {
            font-weight: 300;
            line-height: 1;
            letter-spacing: -.05rem;
            }


            /*CHECKBOX*/
            .radio-style{
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
          }

          .radio-style input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
          }

          .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
          }

          .radio-style:hover input ~ .checkmark {
            background-color: #ccc;
          }

          .radio-style input:checked ~ .checkmark {
            background-color: #ff8c00;
          }

          .checkmark:after {
            content: "";
            position: absolute;
            display: none;
          }

          .radio-style input:checked ~ .checkmark:after {
            display: block;
          }

          .radio-style .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
          }

          /*torna su */
          #tornasu {
            display: inline-block;
            width: 50px;
            height: 50px;
            text-align: center;
            position: fixed;
            bottom: 30px;
            right: 30px;
            /*transition: background-color .3s,*/
            opacity .5s, visibility .5s;
            opacity: 0;
            visibility: hidden;
            z-index: 1000;
          }
          #tornasu.show {
            opacity: 1;
            visibility: visible;
          }

          /*flex container*/
          .flex-container {
            display: flex;
            flex-wrap: wrap;
            /*justify-content: center;
            align-content: space-between;
            justify-content: space-around;*/
          }

          .flex-container > div {
            width: 220px;
            margin: 20px;
          }

          /*CART*/
          .badge {
            padding: 3px 6px 3px 6px;
            -webkit-border-radius: 50px;
            -moz-border-radius: 50px;
            border-radius: 50px;
          }

          #cart_quantity {
              background: #ff8c00;
              color: white;
          }
      </style>


      <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-white site-header sticky-top shadow-sm bg-white rounded">
          <div class="container">
            <a class="navbar-brand bigger" href="{{ URL::asset('/') }}"><img id="logo" src="{{URL::asset('logo.png')}}" width="150" height="50"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
              <ul class="navbar-nav md-auto">

                  @guest
                    <!--utente non autenticato-->
                    <li class="nav-item">
                      <h6 class="text-uppercase font-weight-bold">
                        <a class="nav-link bigger" href="{{ URL::asset('/') }}"><strong>Home</strong></a>
                      </h6>
                    </li>

                    <li class="nav-item">
                      <h6 class="text-uppercase font-weight-bold">
                        <a class="nav-link bigger" href="{{ URL::action('BookController@index') }}"><strong>Libri</strong></a>
                      </h6>
                    </li>

                    <!-- Authentication Links -->
                    <li class="nav-item">
                      <h6 class="text-uppercase font-weight-bold">
                        <a class="nav-link bigger" href="{{ route('login') }}"><strong>Log-in</strong></a>
                      </h6>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                          <h6 class="text-uppercase font-weight-bold">
                            <a class="nav-link bigger" href="{{ route('register') }}"><strong>Registrati</strong></a>
                          </h6>
                        </li>
                    @endif

                    @else
                    <!--utente autenticato-->

                    <!--utente "user" autenticato-->
                    @if (Auth::user()->role == 'user')
                      <li class="nav-item">
                        <h6 class="text-uppercase font-weight-bold">
                          <a class="nav-link bigger" href="{{ URL::asset('/') }}"><strong>Home</strong></a>
                        </h6>
                      </li>

                      <li class="nav-item">
                        <h6 class="text-uppercase font-weight-bold">
                          <a class="nav-link bigger" href="{{ URL::action('BookController@index') }}"><strong>Libri</strong></a>
                        </h6>
                      </li>

                      <li class="nav-item">
                        <h6 class="text-uppercase font-weight-bold">
                          <a class="nav-link bigger" href="{{ URL::action('CartController@index') }}"><strong>Carrello</strong>
                            <span class='badge badge-warning' id='cart_quantity'>{{ count(session('cart')) }}</span>
                          </a>
                        </h6>
                      </li>

                      <li class="nav-item">
                        <h6 class="text-uppercase font-weight-bold">
                          <a class="nav-link bigger" href="{{ URL::action('WishlistController@index') }}"><strong>Wishlist</strong></a>
                        </h6>
                      </li>

                      <!-- Authentication Links -->
                      @guest
                        <li class="nav-item">
                          <h6 class="text-uppercase font-weight-bold">
                            <a class="nav-link bigger" href="{{ route('login') }}"><strong>Log-in</strong></a>
                          </h6>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                              <h6 class="text-uppercase font-weight-bold">
                                <a class="nav-link bigger" href="{{ route('register') }}"><strong>Registrati</strong></a>
                              </h6>
                            </li>
                        @endif
                      @endguest
                    @endif

                    <!--utente "admin" autenticato-->
                    @if (Auth::user()->role == 'admin')
                      <li class="nav-item">
                        <h6 class="text-uppercase font-weight-bold">
                          <a class="nav-link bigger" href="{{ URL::asset('/dashboardBooks') }}"><strong>Gestione libri</strong></a>
                        </h6>
                      </li>
                      <li class="nav-item">
                        <h6 class="text-uppercase font-weight-bold">
                          <a class="nav-link bigger" href="{{ URL::asset('/dashboardOrders') }}"><strong>Gestione ordini</strong></a>
                        </h6>
                      </li>
                    @endif
                  </ul>

                  <ul class="navbar-nav ml-auto">
                    <!--comune a tutti gli utenti autenticati-->
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-uppercase font-weight-bold bigger" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><strong>{{ Auth::user()->username }}</strong></a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if (Auth::user()->role == 'user')
                              <a class="dropdown-item" style="background-color: white" href="{{ URL::asset('myaccount') }}">Il mio account</a>
                            @endif
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" style="background-color: white">Log-out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>

                        </div>
                    </li>
                  </ul>
                @endguest
            </div>
          </div>
        </nav>

        <br>
        <div class="container">
          @yield('content')
        </div>
        <br>
      </div>

      @guest
      <!-- Footer -->
      <footer class="page-footer font-small sticky-top shadow-lg bg-white rounded" style="background-color:white">
          <div class="container">
            <div class="bigger" id="tornasu">
              <p class="float-right"><span class="btn btn-primary"><i data-feather="arrow-up"></i></span></p>
            </div>
            <div class="row text-center d-flex justify-content-center pt-5 mb-3">
              <div class="col-md-2">
                <h6 class="text-uppercase font-weight-bold bigger">
                  <a class="a-color" href="{{ URL::asset('whoare') }}" style="color:black; text-decoration:none"><strong>Chi siamo</strong></a>
                </h6>
              </div>

              <div class="col-md-2">
                <h6 class="text-uppercase font-weight-bold bigger">
                  <a class="a-color" href="{{ URL::asset('aboutus') }}" style="color:black; text-decoration:none"><strong>Su di noi</strong></a>
                </h6>
              </div>

              <div class="col-md-2">
                <h6 class="text-uppercase font-weight-bold bigger">
                  <a class="a-color" href="{{ URL::action('BookController@index') }}" style="color:black; text-decoration:none"><strong>Prodotti</strong></a>
                </h6>
              </div>

              <div class="col-md-2">
                <h6 class="text-uppercase font-weight-bold bigger">
                  <a class="a-color" href="{{ URL::asset('infodelivery') }}" style="color:black; text-decoration:none"><strong>Spedizioni</strong></a>
                </h6>
              </div>

              <div class="col-md-2">
                <h6 class="text-uppercase font-weight-bold bigger">
                  <a class="a-color" href="{{ URL::asset('contactus') }}" style="color:black; text-decoration:none"><strong>Contattaci</strong></a>
                </h6>
              </div>
            </div>

            <hr class="rgba-white-light" style="margin: 0 15%;">

            <div class="row d-flex text-center justify-content-center">
              <div class="col-md-8 col-12 mt-5">
                <blockquote class="blockquote blockquote-reverse">
                  <p>“Nessuna giornata in cui si è imparato qualcosa è andata persa”</p>
                  <footer class="blockquote-footer"><cite title="Source Title">David Eddings</cite></footer>
                </blockquote>
              </div>
            </div>
          </div>
        </footer>
    @endguest


    @if (Auth::check())
      @if(Auth::user()->role == "user")
    <!-- Footer -->
    <footer class="page-footer font-small sticky-top shadow-lg bg-white rounded" style="background-color:white">
        <div class="container">
          <div class="bigger" id="tornasu">
            <p class="float-right"><span class="btn btn-primary"><i data-feather="arrow-up"></i></span></p>
          </div>
          <div class="row text-center d-flex justify-content-center pt-5 mb-3">
            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold bigger">
                <a class="a-color" href="{{ URL::asset('whoare') }}" style="color:black; text-decoration:none"><strong>Chi siamo</strong></a>
              </h6>
            </div>

            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold bigger">
                <a class="a-color" href="{{ URL::asset('aboutus') }}" style="color:black; text-decoration:none"><strong>Su di noi</strong></a>
              </h6>
            </div>

            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold bigger">
                <a class="a-color" href="{{ URL::action('BookController@index') }}" style="color:black; text-decoration:none"><strong>Prodotti</strong></a>
              </h6>
            </div>

            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold bigger">
                <a class="a-color" href="{{ URL::asset('infodelivery') }}" style="color:black; text-decoration:none"><strong>Spedizioni</strong></a>
              </h6>
            </div>

            <div class="col-md-2">
              <h6 class="text-uppercase font-weight-bold bigger">
                <a class="a-color" href="{{ URL::asset('contactus') }}" style="color:black; text-decoration:none"><strong>Contattaci</strong></a>
              </h6>
            </div>
          </div>

          <hr class="rgba-white-light" style="margin: 0 15%;">

          <div class="row d-flex text-center justify-content-center">
            <div class="col-md-8 col-12 mt-5">
              <blockquote class="blockquote blockquote-reverse">
                <p>“Nessuna giornata in cui si è imparato qualcosa è andata persa”</p>
                <footer class="blockquote-footer"><cite title="Source Title">David Eddings</cite></footer>
              </blockquote>
            </div>
          </div>
        </div>

      </footer>
    @endif
  @endif

  <script>
    feather.replace()
  </script>

  <!--<script src=”https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js”></script>-->
  </body>
</html>




<script>

  $(document).ready(function(){

    //visualizzazione dei libri in view('books')
     book_generally();

     function book_generally(page, course='Qualsiasi', condition='Qualsiasi', query = '') {
      $.ajax({
        url:"/books/action?page="+page+"&course="+course+"&condition="+condition+"&query="+query,
       headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
       type:'GET',
       data:{query:query},
       success:function(data){
         $('#result').html(data);
       }
      })
     }

     $(document).on('keyup', '#search', function(){
      var query = $('#search').val();
      var course = $('#course').val();
      var condition = $('#condition').val();
      //var page = $('#hidden_page').val();
      book_generally(1, course, condition, query);
     });

     $(document).on('change', '#course', function(){
      var query = $('#search').val();
      var course = $('#course').val();
      var condition = $('#condition').val();
      //var page = $('#hidden_page').val();
      book_generally(1, course, condition, query);
     });

     $(document).on('change', '#condition', function(){
      var query = $('#search').val();
      var course = $('#course').val();
      var condition = $('#condition').val();
      //var page = $('#hidden_page').val();
      book_generally(1, course, condition, query);
     });



     //visualizzazione dei libri in view('dashboardBooks') (riservata all'admin)
     book_admin();
     function book_admin(page, query = ''){
      $.ajax({
       url:"/dashboardBooks/action?page="+page+"&query="+query,
       headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
       method:'GET',
       data:{query:query},
       success:function(data){
        $('#resultAdmin').html(data);
       }
      })
     }

     $(document).on('keyup', '#search2', function(){
      var query = $('#search2').val();
      //var page = $('#hidden_page').val();
      book_admin(1, query);
     });



     //visualizzazione dei libri in view('dashboard_user') riservata all'utente loggato
     book_user();
     function book_user(query = ''){
      $.ajax({
       url:"{{ URL::action('BookController@actionDashboardUser') }}",
       method:'GET',
       data:{query:query},
       success:function(data){
        $('#resultUser').html(data);
       }
      })
     }

     $(document).on('keyup', '#search3', function(){
      var query = $('#search3').val();
      book_user(query);
     });


     //visualizzazione degli ordini in ('dashboardOrders') riservata all'admin
     order_admin();
     function order_admin(page, query = ''){
      $.ajax({
       url:"/dashboardOrders/action?page="+page+"&query="+query,
       method:'GET',
       data:{query:query},
       success:function(data){
        $('#result_orders_admin').html(data);
       }
      })
     }

     $(document).on('keyup', '#searchOrder', function(){
      var query = $('#searchOrder').val();
      var page = $('#hidden_page').val();
      order_admin(page, query);
     });


    //paginazione con ajax
     $(document).on('click', '.pagination a', function(event){
     event.preventDefault();
     var page = $(this).attr('href').split('page=')[1];
     $('#hidden_page').val(page);

     var course = $('#course').val();
     var condition = $('#condition').val();
     var query = $('#search').val();

     $('li').removeClass('active');
     $(this).parent().addClass('active');
     $('html, body').animate({scrollTop:0}, '50');

     book_generally(page, course, condition, query);
     book_admin(page, query);
     order_admin(page, query);
    });

  });


  //CARRELLO

  //aggiunta al carrello
  $(".add-to-cart").click(function (e) {
    var ele = $(this);
    $.ajax({
      url: '{{ url('add-to-cart') }}',
      type: "GET",
      data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
      success: function (response) {
        Swal.fire({
          type: 'success',
          title: "Carrello aggiornato con successo!",
          timer: 1500,
          showConfirmButton: false,
        })
        $("#cart_quantity").html(response['articles']);
      }
    });
  });

  //aggiornamento quantità carrello
  $(".update-cart").change(function (e) {
     e.preventDefault();
     var ele = $(this);
      $.ajax({
         url: '{{ url('update-cart') }}',
         method: "POST",
         data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
         success: function (response) {
           if(response['state'] == "true"){
             Swal.fire({
               type: 'success',
               title: "Carrello aggiornato con successo!",
               timer: 1500,
               showConfirmButton: false,
             })
             $(".quantity").html(response['value']);
             $(".subtotal").html(response['subtotal']);
             $(".spedizione").html(response['spedizione']);
             $(".total").html(response['total']);
           }

           if(response['state'] == "false"){
             Swal.fire({
               type: 'warning',
               title: "Ci dispiace, non abbiamo a disposizione tutte queste copie!",
               timer: 1500,
               showConfirmButton: false,
             })
             $(ele).val(response['value']);
             $(".subtotal").html(response['subtotal']);
             $(".spedizione").html(response['spedizione']);
             $(".total").html(response['total']);
           }
         }
      });
  });

  //rimozione elemento dal carrello
  $(".remove-from-cart").click(function (e) {
      e.preventDefault();
      var ele = $(this);
      Swal.fire({
        title: 'Sei sicuro di voler rimuovere questo elemento dal carrello?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'gray',
        cancelButtonColor: '#ff8c00',
        confirmButtonText: 'Sì, sono sicuro!',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          $.ajax({
              url: '{{ url('remove-from-cart') }}',
              method: "DELETE",
              data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
              success: function (response) {
                if(response['state'] == "true"){
                  //se rimuovendo l'elemento il carrello è vuoto
                  if(response['articles'] <= 0){
                    Swal.fire({
                      type: 'success',
                      title: "Elemento rimosso con successo!",
                      timer: 1500,
                      showConfirmButton: false,
                    })
                    window.location.reload();
                  }
                  //se restano ulteriori elementi nel carrello
                  if(response['articles'] > 0){
                    $(".element" + ele.attr("data-id")).remove();
                    $("#quantity_cart").html(response['articles']);
                    Swal.fire({
                      type: 'success',
                      title: "Elemento rimosso con successo!",
                      timer: 1500,
                      showConfirmButton: false,
                    })
                    $("#cart_quantity").html(response['articles']);
                    $(".subtotal").html(response['subtotal']);
                    $(".spedizione").html(response['spedizione']);
                    $(".total").html(response['total']);
                  }
                }
                if(response['state'] == "false"){
                  Swal.fire({
                    type: 'warning',
                    title: "Ci dispiace, sembra che sia impossibile rimuovere questo elemento dal carrello! Prova a riaggiornare la pagina",
                    timer: 1500,
                    showConfirmButton: false,
                  })

                }
              }
          });
        }
      })
  });


  //WISHLIST

  //aggiunta alla wishlist
  $(".add-to-wishlist").click(function (e) {
    var ele = $(this);
    $.ajax({
      url: '{{ url('add-to-wishlist') }}',
      type: "GET",
      data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
      success: function (response) {
        //se l'articolo è già nella lista dei desideri
        if(response['state'] == "true"){
          Swal.fire({
            type: 'warning',
            title: "Hai già inserito questo articolo nella wishlist!",
            timer: 1500,
            showConfirmButton: false,
          })
        }

        //se l'articolo NON è già nella lista dei desideri
        if(response['state'] == "false"){
          Swal.fire({
            type: 'success',
            title: "Wishlist aggiornata con successo!",
            timer: 1500,
            showConfirmButton: false,
          })
        }
      }
    });
  });


  //rimozione elemento dalla wishlist
  $(".remove-from-wishlist").click(function (e) {
      e.preventDefault();
      var ele = $(this);
      $.ajax({
          url: '{{ url('remove-from-wishlist') }}',
          method: "DELETE",
          data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
          success: function (response) {
            if(response['state'] == "true"){
              //se rimuovendo l'elemento il carrello è vuoto
              if(response['articles'] <= 0){
                Swal.fire({
                  type: 'success',
                  title: "Elemento rimosso con successo!",
                  timer: 1500,
                  showConfirmButton: false,
                })
                window.location.reload();
              }
              //se restano ulteriori elementi nel carrello
              if(response['articles'] > 0){
                $(".element" + ele.attr("data-id")).remove();
                $("#quantity_wishlist").html(response['articles']);
                Swal.fire({
                  type: 'success',
                  title: "Elemento rimosso con successo!",
                  timer: 1500,
                  showConfirmButton: false,
                })
              }
            }
            if(response['state'] == "false"){
              Swal.fire({
                type: 'warning',
                title: "Ci dispiace, sembra che sia impossibile rimuovere questo elemento dalla wishlist! Prova a riaggiornare la pagina",
                timer: 1500,
                showConfirmButton: false,
              })

            }
          }
      });
  })


  //visualizzazione messaggio di successo con sweetalert
   var msg = '{{Session::get('success')}}';
   var exist = '{{Session::has('success')}}';
   if(exist){
     Swal.fire({
       type: 'success',
       title: msg,
       timer: 1500,
       showConfirmButton: false,
     })
   }

   //visualizzazione messaggio di insuccesso con sweetalert
    var msg = '{{Session::get('error')}}';
    var exist = '{{Session::has('error')}}';
    if(exist){
      Swal.fire({
        type: 'warning',
        title: msg,
        timer: 1500,
        showConfirmButton: false,
      })
    }

  //spostamento fluido ad un altro punto della pagina
   function scroll_to(id) {
    $('html,body').animate({
      scrollTop: $('#'+id).offset().top
    },'slow');
  }


  jQuery(document).ready(function() {

    var btn = $('#tornasu');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 50) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '50');
    });

  });



</script>
