@extends ('layout.app')

@section('content')

<br>

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div align="center">
  <h3>Contattaci</h3>
  </div>

  <section class="mb-4" style="width:50%;  align:center; margin:auto">


      <p class="text-center w-responsive mx-auto mb-5" ><h4 align="center">Avete domande? Non esitate a contattarci!<br><br></h4></p>
          <!--Grid column-->
      <div class="text-center w-responsive mx-auto mb-5" >
          <ul class="list-unstyled mb-0">
              <li> <i data-feather="map-pin"></i>
                  <p>Via Saragat 1, Ferrara, FE 44121, Italy<br><br></p>
              </li>

              <li> <i data-feather="phone"></i>
                  <p>+ 39 234 567 89<br><br></p>
              </li>

              <li> <i data-feather="mail"></i>
                  <p>contact@unibook.com</p>
              </li>
          </ul>
      </div>
      <!--Grid column-->
  </section>
  <br>
</div>







@endsection
