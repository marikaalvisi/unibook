@extends('layout.app')

@section('content')

<div class="row">
    <h1>Modifica i dati utente</h1>

    <div class="col-sm-6">
        <form action="{{ URL::asset('UserController@update') }}" method="POST">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" class="form-control" value="{{$user->name}}" id="name" name="name">
            </div>


            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" value="{{$user->email}}" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="password">password</label>
                <input type="password" class="form-control" value="{{$user->password}}" id="password" name="password">
            </div>

            <!--immagine-->
            <div class="form-group">
              <label for="image">Foto profilo</label>
              <input type="file" class="form-control-file" id="image" name="image">
            </div>

            <input type="submit" class="btn btn-dark" value="Invia"></input>
        </form>
    </div>
</div>

@endsection
