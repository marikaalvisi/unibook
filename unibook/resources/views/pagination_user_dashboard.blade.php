@if (isset($data) && count($data) > 0)


 @foreach($data as $row)
   <div class="container box">
      <div class="row">
         <div class="col-sm-4" >
         <!--foto libro-->
           <img src="{{ url('images/'.$row->image) }}" class="rounded float-left" width="200" height="275" alt="{{$row->title}}">
         </div>

         <div class="col-sm-6">
           <!--info libro-->
           <table class="table table-sm">
             <tbody>
               <tr>
                 <th scope="row">Titolo</th>
                 <td>{{$row->title}}</td>
               </tr>
               <tr>
                 <th scope="row">Autore</th>
                 <td>{{$row->author}}</td>
               </tr>
               <tr>
                 <th scope="row">Corso</th>
                 <td>{{$row->course}}</td>
               </tr>
               <tr>
                 <th scope="row">Editore</th>
                 <td>{{$row->editor}}</td>
               </tr>
               <tr>
                 <th scope="row">Edizione</th>
                 <td>{{$row->edition}}</td>
               </tr>
               <tr>
                 <th scope="row">Pagine</th>
                 <td>{{$row->pages}}</td>
               </tr>
               <tr>
                 <th scope="row">Prezzo</th>
                 <td>{{ number_format((float)$row->price , 2, '.', '')}}€</td>
               </tr>
               <tr>
                 <th scope="row">Descrizione</th>
                 <td>{{$row->description}}</td>
               </tr>
             </tbody>
           </table>
           <br>

           <!--se il libro è stato venduto-->
           @if ($row->availability == 0)
           <h6 style="color:#ff8c00">Qualcuno ha acquistato il tuo libro!</h6>

           <!--calcolo del prezzo meno il 10%-->
           @php
            $price = $row->price - (($row->price/100)*10);
           @endphp

           <p style="color:gray">Riceverai {{ number_format((float)$price , 2, '.', '')}}€ direttamente dal corriere al momento del ritiro</p>
           @endif
       </div>

       <!--se il libro NON è ancora stato venduto-->
       @if ($row->availability > 0)
       <div class="col-sm-2">
         <!--crud-->
         <div class="row">
          <div class="bigger">
            <a class="btn btn-link a-color" href="{{URL::action('BookController@edit', $row->id)}}" style="text-decoration:none; color:gray" align="right" >Modifica <i data-feather="edit-3"></i></a>
          </div>
         </div>
         <div class="row">
          <div class="bigger">
            <a class="btn btn-link a-color" href="{{URL::action('BookController@destroy', $row->id)}}" style="text-decoration:none; color:gray" align="right" id="remove">Elimina <i data-feather="trash-2"></i></a>
          </div>
         </div>
       </div>
       @endif

     </div>
   </div>
  <hr>
 @endforeach

 @else
 <tr>
  <td align="center" colspan="5">Nessun libro trovato</td>
 </tr>

@endif



<script>
  feather.replace()
</script>
