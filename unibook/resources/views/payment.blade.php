@extends('layout.app')

@section('content')

  <div class="container shadow rounded"style="background-color:white">
    <nav class="breadcrumb" style="background-color:white;">
      <span class="breadcrumb-item" style="color:gray">Indirizzo</span>
      <span class="breadcrumb-item active" style="color:#ff8c00; text-decoration:none" href="">Pagamento</span>
      <span class="breadcrumb-item" style="color:gray; text-decoration:none" href="">Riepilogo</span>
    </nav>
  </div>

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <div class="container">
    <h3>Metodo di pagamento</h3>
    <br>
    <div class="container">

      <!--pagamento con carta di credito-->
      <form action="{{ URL::action('PurchaseController@addPayment') }}" method="POST">


      <label class="radio-style"><h5><i data-feather="credit-card"></i> Carta di credito</h5>
        <input  type="radio" name="payment" id="creditcard" value="creditcard" checked>
        <span class="checkmark"></span>
      </label>

      <br>
      <div class="container">

        @if (count($errors) > 0)
          <script>
            Swal.fire({
              type: 'warning',
              title: 'Oops...',
              text: 'Sembra che tu non abbia inserito correttamente i campi!',
              showConfirmButton: false,
              timer: 2000,
            })
          </script>
        @endif


            {{ csrf_field() }}

            <!--Nome titolare-->
            <div class="form-group">
                <label for="name">Nome del titolare <strong style="color:#ff8c00">*</strong></label>
                <input type="text" class="form-control" id="surname" name="name">
            </div>

            <!--Numero carta-->
            <div class="form-group">
                <label for="cardnumber">Numero carta <strong style="color:#ff8c00">*</strong></label>
                <input type="text" class="form-control" id="cardnumber" name="cardnumber">
            </div>

            <!--Data di scadenza-->
            <div class="form-group">
                <label for="date">Data di scadenza <strong style="color:#ff8c00">*</strong></label>
                <input type="text" class="form-control" id="date" name="date" placeholder="MM/YY">
            </div>

            <!--Data di scadenza-->
            <div class="form-group">
                <label for="CVV" >Codice di verifica <strong style="color:#ff8c00">*</strong></label>
                <div class="row">
                  <div class="col-11">
                    <input type="text" class="form-control" id="CVV" name="CVV">
                  </div>
                  <div class="col-1">
                    <img src="{{URL::asset('cvv-visa-1x.png')}}" width="70" height="30">
                  </div>
                </div>
            </div>

        </div>

        <br>
        <hr>
        <br>

        <!--pagamento in contanti alla consegna-->

        <label class="radio-style"><h5><i data-feather="truck"></i> Pagamento alla consegna</h5>
          <input type="radio" name="payment" id="money" value="money">
          <span class="checkmark"></span>
        </label>

        <br>
        <div class="container">


              {{ csrf_field() }}

              @php
              $address = session('address');
               @endphp
              <h6>Spedizione presso:</h6>
              <div class="container">
                <p>{{$address['name']}} {{$address['surname']}}<br>{{$address['address']}}<br>{{$address['CAP']}}<br>{{$address['city']}}<br></p>
              </div>

              <p style="color:gray">Il pagamento alla consegna è sempre gratuito! Prepara l’importo esatto: il vettore non può dare il resto.</p>

              <br>
              <hr>
              <br>

              <div class="grow">
                <button type="submit" class="btn btn-primary">Vai al riepilogo ordine<i data-feather="arrow-right"></i></button>
              </div>
              <br>
              <div class="grow">
                <a href="{{ URL::action('PurchaseController@editAddress') }}" class="btn btn-secondary" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
              </div>
          </form>
        </div>
      </div>
  </div>
</div>

@endsection
