@extends ('layout.app')
@section('content')
  
<div class="container shadow p-3 mb-5 rounded" style="background-color:white">

 <br>
 <div class="row">
   <div class="col-lg-10">
     <h3 align="center">Gestione libri di testo</h3>
   </div>

   <div class="col-lg-2">
     <div class="grow">
       <button type="button" class="btn btn-primary" style="text-decoration:none" data-toggle="modal" data-target="#exampleModalScrollable">
         Aggiungi libro <i data-feather="plus"></i>
       </button>
     </div>
   </div>
  </div>
 <br>
 <div class="panel panel-default">
  <div class="panel-body">
   <div class="form-group">
    <input type="text" name="search2" id="search2" class="form-control" placeholder="inserisci codice del libro, titolo, autore o corso di studi">
   </div>
   <br>

   <div id='resultAdmin'>
     @include('pagination_dashboard')
     <!--ajax, vedere AdminController@action-->
   </div>

   <!-- Modal -->
   <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-scrollable" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="exampleModalScrollableTitle">Nuovo libro</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body">

           @if (count($errors) > 0)
             <script>
               Swal.fire({
                 type: 'warning',
                 title: 'Oops...',
                 text: 'Sembra che tu non abbia inserito correttamente i campi!',
                 showConfirmButton: false,
                 timer: 1500,
               })
             </script>
           @endif

           <form action="{{ URL::action('BookController@storeAdmin') }}" method="POST" id="form">

               {{ csrf_field() }}

               <!--titolo-->
               <div class="form-group">
                   <label for="title">Titolo <strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="title" name="title">
               </div>

               <!--autore-->
               <div class="form-group">
                   <label for="author">Autore <strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="author" name="author">
               </div>

               <!--corso-->
               <div class="form-group">
                   <label for="author">Corso di studi <strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="course" name="course">
               </div>

               <!--immagine-->
               <div class="form-group">
                 <label for="image">Immagine</label>
                 <input type="file" class="form-control-file" id="image" name="image">
               </div>

               <!--Descrizione-->
               <div class="form-group">
                 <label for="description">Descrizione</label>
                 <textarea class="form-control" id="description" name="description"></textarea>
               </div>

               <!--prezzo-->
               <div class="form-group">
                   <label for="author">Prezzo <span style="color:gray">(Prezzo massimo: 999.99€)</span><strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="price" name="price">
               </div>

               <!--editore-->
               <div class="form-group">
                   <label for="author">Editore <strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="editor" name="editor">
               </div>

               <!--edizione-->
               <div class="form-group">
                   <label for="author">Edizione <strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="edition" name="edition">
               </div>

               <!--pagine-->
               <div class="form-group">
                   <label for="author">Pagine <strong style="color:#ff8c00">*</strong></label>
                   <input type="text" class="form-control" id="pages" name="pages">
               </div>

               <!--Disponibilità-->
               <div class="form-group">
                   <label for="author">Pezzi attualmente disponibili</label>
                   <input type="text" class="form-control" id="availability" name="availability">
               </div>

               <div class="grow">
                 <button type="submit" class="btn btn-primary">Aggiungi <i data-feather="plus"></i></button>
               </div>

               <br>

               <div class="grow">
                 <a href="{{ URL::action('BookController@dashboard') }}" class="btn btn-secondary" data-dismiss="modal" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
               </div>

             </form>
           </div>
         </div>
       </div>
     </div>

  </div>
 </div>
</div>
@endsection
