@extends ('layout.app')
@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">

 <br>
  <h3 align="center">Gestione ordini</h3>
 <br>
 <div class="panel panel-default">
  <div class="panel-body">
   <div class="form-group">
    <input type="text" name="searchOrder" id="searchOrder" class="form-control" placeholder="inserisci il numero d'ordine">
   </div>
   <br>

   <div id='result_orders_admin'>
     @include('pagination_orders_admin')
   </div>

  </div>
 </div>
</div>

@endsection
