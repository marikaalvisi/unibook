@extends ('layout.app')
@section('content')

<div class="container wishlist">
  <!--se è presente qualcosa nel carrello-->
  @if (Auth::user()->wishlist->count() > 0)
  <div class="row">
    <!--lista dei desideri-->
    <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
      <div class="container">
        <h3>La tua wishlist</h3>
        <h6 style="color:gray">Articoli nella wishlist: <span id="quantity_wishlist">{{Auth::user()->wishlist->count() }}<span></h6>

        <!--se la lista NON è vuota-->
        @foreach($wishlists as $wishlist)
          <div class=" element{{$wishlist->id}}">
              <hr>
                <table id="wishlist" class="table-responsive">
                  <thead>
                  <tr>
                      <th scope="col" style="width:20%"></th>
                      <th scope="col" style="width:69%"></th>
                      <th scope="col" style="width:11%"></th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                       <td  data-th="Immagine" align="left">
                         <a href="{{ URL::action('BookController@show', $wishlist->book->id) }}">
                           <img src="{{ url('images/'.$wishlist->book->image) }}" width="150" height="200">
                         </a>
                       </td>
                       <td data-th="Prodotto">
                         <a href="{{ URL::action('BookController@show', $wishlist->book->id) }}" style="text-decoration:none; color:black">
                           <div class="row">
                             <h6>{{$wishlist->book->title}}</h6>
                           </div>
                           <div class="row">
                             <p style="color:gray">{{$wishlist->book->author}}</p>
                           </div>
                          </a>
                         <div class="row">
                           <h6>{{ number_format((float)$wishlist->book->price, 2, '.', '')}}€</h6>
                         </div>
                       </td>
                       <td class="actions" data-th="">
                           <button class="btn btn-link remove-from-wishlist bigger a-color" style="color:gray;text-decoration:none" data-id="{{$wishlist->id}}">Rimuovi<i data-feather="trash-2"></i></button>
                       </td>
                     </tr>
                 </tbody>
               </table>
           </div>
          @endforeach
        </div>
        <br>
     </div>
  </div>


  <!--se non c'è nessun articolo nella wishlist-->
  @else
  <div class="row">
      <div class="container shadow p-3 mb-5 rounded" style="background-color:white">
        <div class="container">
          <h3>La tua wishlist</h3>
          <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center">
            <div class="col-md-5 p-lg-5 mx-auto my-5">
              <br>
              <br>
              <h4>Ops, sembra che non ci sia ancora niente nella tua wishlist!</h4>
              <br>
            </div>
          </div>
        </div>
        <br>
      </div>
    </div>
  @endif
</div>

@endsection
