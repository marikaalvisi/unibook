@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div class="container">

    <br>
    <div class="row">
      <div class="col-sm-3">
        <h6>Il mio account</h6>
        <div class="container">
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/profile') }}">Il mio profilo</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/myorders') }}">I miei ordini</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/mybooks') }}">I miei libri</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/credentials') }}">Opzioni di accesso</a>
        </div>
      </div>

      <div class="col-sm-9">
        <!--breadcrumb-->
        <a class="breadcrumb-item" style="color:black" href="{{ URL::asset('/myaccount') }}">Il mio account</a>
        <span class="breadcrumb-item active">I miei ordini</span>

        <br>
        <br>
        <h3>I miei ordini</h3>
        <p>Qui puoi visualizzare tutti i tuoi ordini</p>

        <br>


        @if (count($orders) == 0)
          <h6>Ops, sembra che tu non abbia ancora effettuato nessun ordine!</h6>
          <br>
          <p><a class="btn btn-primary bigger" href="{{ URL::action('BookController@index') }}" role="button">Inizia ad acquistare <i data-feather="plus"></i></a></p>

        @else

          @foreach ($orders as $order)
            <h6 style="color:#ff8c00">Ordine #{{$order->id}}</h6>
            <div class="container">
              <p>{{$order->created_at}}</p>

            @php
              $string = $order->order;
              $token = strtok($string, "@");
            @endphp
              @while  ($token !== false)
                <p>{{$token}}</p>
                @php
                  $token = strtok("@");
                @endphp
              @endwhile

              <h6>{{number_format((float)$order->total, 2, '.', '')}}€</h6>
              <h6>Ordine {{$order->order_status}}</h6>
            </div>

            <hr>
          @endforeach
        @endif
        <br>

      </div>
    </div>
  </div>
  <br>
</div>

@endsection
