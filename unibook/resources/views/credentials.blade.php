@extends ('layout.app')

@section('content')

<div class="container shadow p-3 mb-5 rounded" style="background-color:white">
  <br>
  <div class="container">

    <br>

    <div class="row">
      <div class="col-sm-3">
        <h6>Il mio account</h6>
        <div class="container">
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/profile') }}">Il mio profilo</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/myorders') }}">I miei ordini</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/mybooks') }}">I miei libri</a>
          <a class="nav-link grow a-color" style="color:black" href="{{ URL::asset('/myaccount/credentials') }}">Opzioni di accesso</a>
        </div>
      </div>


      <div class="col-sm-9">

        <!--breadcrumb-->
        <a class="breadcrumb-item" style="color:black" href="{{ URL::asset('/myaccount') }}">Il mio account</a>
        <span class="breadcrumb-item active">Opzioni di accesso</span>

        <br>
        <br>

        <h3>Opzioni di accesso</h3>
        <p>Da qui puoi gestire le tue opzioni di accesso all'account.</p>

        <br>
        <div class="table-responsive-sm">
          <table class="table table-borderless">
            <thead>
              <tr>
                <th scope="col"><i data-feather="mail"></i> Email</th>
                <th scope="col"><i data-feather="key"></i> Password</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ Auth::user()->email }}</td>
                <td>********</td>
              </tr>
              <tr>
                <td>
                  <div class="grow">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalEmail" style="text-decoration:none">
                      Modifica email <i data-feather="arrow-right"></i>
                    </button>
                  </div>
                </td>
                <td>
                  <div class="grow">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalPassword" style="text-decoration:none">
                      Modifica password <i data-feather="arrow-right"></i>
                    </button>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>


    <!-- Modal email-->
    <div class="modal fade" id="exampleModalEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modifica il tuo indirizzo email </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="{{ URL::action('UserController@updateEmail', Auth::user()->id)}}" method="POST" id="form">
                {{ csrf_field() }}

                <h5>Email corrente</h5>
                <p>{{Auth::user()->email}}</p>

                <div class="form-group">
                    <label for="email">Nuova email <strong style="color:#ff8c00">*</strong></label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>

                <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                   <label for="password">Password <strong style="color:#ff8c00">*</strong></label>
                   <input id="password" type="password" class="form-control" name="password" required>

                   @if ($errors->has('password'))
                       <span class="help-block">
                           <strong>{{ $errors->first('password') }}</strong>
                       </span>
                   @endif
               </div>


                <div class="grow">
                  <button type="submit" class="btn btn-primary">Aggiorna <i data-feather="refresh-cw"></i></button>
                </div>

                <br>

                <div class="grow">
                  <a class="btn btn-secondary" href="{{ URL::asset('myaccount/credentials') }}" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
                </div>

            </form>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal password-->
    <div class="modal fade" id="exampleModalPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modifica la tua password </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            @if (count($errors) > 0)
              <script>
                Swal.fire({
                  type: 'warning',
                  title: 'Oops...',
                  text: 'Sembra che tu non abbia inserito correttamente i campi!',
                  showConfirmButton: false,
                  timer: 2000,
                })
              </script>
            @endif

            <form action="{{ URL::action('UserController@updatePassword', Auth::user()->id)}}" method="POST" >
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                   <label for="new-password">Password attuale <strong style="color:#ff8c00">*</strong></label>
                   <input id="current-password" type="password" class="form-control" name="current-password" required>

                   @if ($errors->has('current-password'))
                       <span class="help-block">
                           <strong>{{ $errors->first('current-password') }}</strong>
                       </span>
                   @endif
               </div>

               <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                   <label for="new-password">Nuova password <strong style="color:#ff8c00">*</strong></label>
                   <input id="new-password" type="password" class="form-control" name="new-password" required>

                   @if ($errors->has('new-password'))
                       <span class="help-block">
                           <strong>{{ $errors->first('new-password') }}</strong>
                       </span>
                   @endif
               </div>

               <div class="form-group">
                   <label for="new-password-confirm">Conferma nuova password <strong style="color:#ff8c00">*</strong></label>
                   <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
               </div>

                <div class="grow">
                  <button type="submit" class="btn btn-primary">Aggiorna <i data-feather="refresh-cw"></i></button>
                </div>

                <br>

                <div class="grow">
                  <a class="btn btn-secondary" href="{{ URL::asset('myaccount/credentials') }}" style="text-decoration:none"><i data-feather="arrow-left"></i> Indietro</a>
                </div>

            </form>
          </div>
        </div>
      </div>
    </div>


  </div>
  <br>
</div>

@endsection
